//
//  WidgetCouponCell.swift
//  UmmaBee
//
//  Created by ISPG on 30/01/20.
//  Copyright © 2020 ISPG. All rights reserved.
//

import UIKit

protocol WidgetCouponCellDelegate  {
    func showDetails(widgetItemslistwidgetModel:WidgetItemslistwidgetModel?)
    func ListAllCoupons(widgetslistwidgetModel:WidgetslistwidgetModel?)

}

class WidgetCouponCell: UITableViewCell {
    
    @IBOutlet weak var viewAllBtn: UIButton!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var widgetTitle: UILabel!
    @IBOutlet weak var widgetIcon: UIImageView!
    @IBOutlet weak var colView: UICollectionView!
    
//    var arrData = [WidgetItemslistwidgetModel]()
//    var widgetslistwidgetModel:WidgetslistwidgetModel?
    var delegate : WidgetCouponCellDelegate?
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.widgetTitle.textColor = UIColor.COLOR_DARK_BLACK
        colView.register(UINib(nibName: "WidgetCouponItemCell", bundle: .main), forCellWithReuseIdentifier: "ItemCouponCell")

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
        
    
    func configureCell(widgetslistwidgetModel:WidgetslistwidgetModel?) {
        self.widgetslistwidgetModel = widgetslistwidgetModel
        
        if let widgetIconUrl = self.widgetslistwidgetModel?.widgetIcon {
            self.widgetIcon?.sd_setImage(with: URL(string: widgetIconUrl), placeholderImage: UIImage(named: DFImageDefault))
        }
        
        let viewMore = widgetslistwidgetModel?.viewMoreLabel ?? ""
        if viewMore == "" {
            self.viewAllBtn.isHidden = true
        } else {
            self.viewAllBtn.isHidden = false
        }
        
        let showColor = widgetslistwidgetModel?.showColor ?? false
        if showColor == true {
            
            //self.baseView.backgroundColor = UIColor.white//UIColor.colorWithHexString("#323044")
            //self.viewAllBtn.setTitleColor(UIColor.white, for: .normal)
           // self.viewAllBtn.layer.borderColor = UIColor.white.cgColor
            //self.widgetTitle.textColor = UIColor.white
            
        } else {
           // self.baseView.backgroundColor = UIColor.white
            //self.viewAllBtn.setTitleColor(UIColor.black, for: .normal)
           // self.viewAllBtn.layer.borderColor = UIColor.black.cgColor
            //self.widgetTitle.textColor = UIColor.black
            
        }
    
        self.arrData = widgetslistwidgetModel?.widgetItems?[0] ?? []
        self.widgetTitle.text = widgetslistwidgetModel?.widgetTitle ?? ""

        
        let viewMoreLabel = widgetslistwidgetModel?.viewMoreLabel ?? ""
        if viewMoreLabel != "" {
            self.viewAllBtn.setTitle(viewMoreLabel, for: .normal)
        }
        
        DispatchQueue.main.async {
            self.colView.reloadData()
            
        }
    }
    
    
    @IBAction func viewAllBtn(_ sender: Any) {
        
        if let widgetslistwidgetModel = self.widgetslistwidgetModel{
            self.delegate?.ListAllCoupons(widgetslistwidgetModel: widgetslistwidgetModel)
        }
        
    }
}



extension WidgetCouponCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return widgetslistwidgetModel?.widgetItems?[0].count ?? 0
      }
      
      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          
          if let moduleType = widgetslistwidgetModel?.moduleType {
              if moduleType == "voucher" {
                  let listingType = widgetslistwidgetModel?.listingType
                  let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemCouponCell", for: indexPath) as! WidgetCouponItemCell
                  //  cell.setBaseViewViewDropShadow()
                  let index =  widgetslistwidgetModel?.widgetItems?[0]
                  cell.delegate = self

                  cell.configureCell(widgetItemslistwidgetModel: index?[indexPath.row])
                  return cell
              }
              
          }
         
          return UICollectionViewCell()
      }
      
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          
          let screenWidth = WIDGET_COUPON_ITEM_WIDTH//200
          let scaleFactor = WIDGET_COUPON_ITEM_HEIGHT//255
          return CGSize(width: screenWidth, height: scaleFactor)
      }
}


extension WidgetCouponCell: ItemCouponCellDelegate {
    func couponDetailWidget(widgetItemslistwidgetModel: WidgetItemslistwidgetModel?) {
        self.delegate?.showDetails(widgetItemslistwidgetModel: widgetItemslistwidgetModel)

    }
    
    func addCouponToWishList(voucherItemList: VoucherListVoucherListResponse?) {
        
    }
    
    func couponShowDetailList(voucherItemList: VoucherListVoucherListResponse?) {
    }
    
    
}
