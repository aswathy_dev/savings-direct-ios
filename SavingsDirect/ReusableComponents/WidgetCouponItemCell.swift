//
//  WidgetCouponItemCell.swift
//  UmmaBee
//
//  Created by ISPG on 30/01/20.
//  Copyright © 2020 ISPG. All rights reserved.
//

import UIKit

protocol  ItemCouponCellDelegate {
    func couponDetailWidget(widgetItemslistwidgetModel:WidgetItemslistwidgetModel?)
    func addCouponToWishList(voucherItemList:VoucherListVoucherListResponse?)
    func couponShowDetailList(voucherItemList:VoucherListVoucherListResponse?)

}

class WidgetCouponItemCell: UICollectionViewCell {
    
    var delegate : ItemCouponCellDelegate?
    var voucherListVoucherListResponse: VoucherListVoucherListResponse?
    var widgetItemslistwidgetModel:WidgetItemslistwidgetModel?

    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var lblInstore: UILabel!
    @IBOutlet weak var lblOnline: UILabel!
    @IBOutlet weak var lblCouponName: UILabel!
    @IBOutlet weak var imgCoupon: UIImageView!
    @IBOutlet weak var imgFavourite: UIImageView!
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var endDate: UILabel!
    @IBOutlet weak var btnShowCoupon: UIButton!
    @IBOutlet weak var btnAddToWallet: UIButton!
    @IBOutlet weak var baseViewFavourite: UIView!
    @IBOutlet weak var baseViewSep: UILabel!
    @IBOutlet weak var showButtonHeight: NSLayoutConstraint?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //lblInstore.textColor = UIColor.COLOR_RED_L

        showButtonHeight?.constant = CGFloat(APP_COMMON_BUTTON_HEIGHT)
        lblOnline.textColor = UIColor.COLOR_GREEN_L
        lblInstore.textColor = UIColor.COLOR_RED
        lblCouponName.textColor = UIColor.COLOR_DARK_BLACK
    }
       
    func configureCellCouponList(voucherListItem: VoucherListVoucherListResponse?) {
        businessName.textColor = UIColor.COLOR_GRAY_L
        endDate.textColor = UIColor.COLOR_ORANGE
        btnShowCoupon.layer.cornerRadius = CGFloat(APP_COMMON_BUTTON_RADIUS)
        
        self.voucherListVoucherListResponse = voucherListItem
        
        if let voucehrType = voucherListItem?.voucherTypes {
            if voucehrType == "Sale" {
                btnShowCoupon.setTitle("Show Coupon", for: .normal)
                
            } else {
                btnShowCoupon.setTitle("Show Coupon", for: .normal)

            }
        }
        
        if let businessLogo = voucherListItem?.itemBusinessLogo {
            self.imgCoupon.loadImage(path: businessLogo, placeHolder: "widgetNoImg")
            
        } else {
            self.imgCoupon.image = UIImage.init(named: "widgetNoImg")
        }

        if let businessName = voucherListItem?.businessName {
            self.businessName.text = businessName
        }
        
        if let couponTitile = voucherListItem?.voucherShortTitle {
            self.lblCouponName.text = couponTitile
        }
        
        if let endDate = voucherListItem?.voucherListingEndDate {
            self.endDate.text = "Ends \(endDate.convertDateFormater(endDate, serverDate: "yyyy-MM-dd hh:mm:ss", returnFormat: "EEEE, MMM dd yyyy"))"
        }
        
        let redeemOnline = voucherListItem?.redeemOnline ?? ""
        let redeemStore = voucherListItem?.redeemStore ?? ""

        if redeemOnline == "Yes" && redeemStore == "Yes"{
            lblInstore.isHidden = false
            lblOnline.isHidden = false
            baseViewSep.isHidden = false
        }
        if redeemOnline == "Yes" && redeemStore == "No"{
            lblInstore.isHidden = true
            lblOnline.isHidden = false
            baseViewSep.isHidden = true
        }
        if redeemOnline == "No" && redeemStore == "Yes"{
            lblInstore.isHidden = false
            lblOnline.isHidden = true
            baseViewSep.isHidden = true
        }
        
        if let wishlist = voucherListItem?.walletItemID {
            self.imgFavourite.image = UIImage.init(named: "wishlist_g")
            self.baseViewFavourite.backgroundColor = UIColor.COLOR_FAV_ADDED
            
        } else {
            self.imgFavourite.image = UIImage.init(named: "wishlist")
            self.baseViewFavourite.backgroundColor = UIColor.COLOR_FAV_NORMAL
       }
        
    }

    
    
    func configureCell(widgetItemslistwidgetModel:WidgetItemslistwidgetModel?) {
        self.widgetItemslistwidgetModel = widgetItemslistwidgetModel
            
        if let couponImageUrl = self.widgetItemslistwidgetModel?.itemBusinessLogo {
            self.imgCoupon.loadImage(path: couponImageUrl, placeHolder: "widgetNoImg")
           // self.imgCoupon?.sd_setImage(with: URL(string: couponImageUrl), placeholderImage: UIImage(named: DFImageDefault))
        }
        
        if let couponTitile = widgetItemslistwidgetModel?.itemName {
            self.lblCouponName.text = couponTitile
        }
        
        let redeemOnline = widgetItemslistwidgetModel?.redeemOnline ?? ""
        let redeemStore = widgetItemslistwidgetModel?.redeemStore ?? ""
//        var redeem  = ""
        if redeemOnline == "Yes" && redeemStore == "Yes"{
            lblInstore.isHidden = false
            lblOnline.isHidden = false
            baseViewSep.isHidden = false
        }
        if redeemOnline == "Yes" && redeemStore == "No"{
            lblInstore.isHidden = true
            lblOnline.isHidden = false
            baseViewSep.isHidden = true
        }
        if redeemOnline == "No" && redeemStore == "Yes"{
            lblInstore.isHidden = false
            lblOnline.isHidden = true
            baseViewSep.isHidden = true
        }
        
//        self.lblOnline.text = redeem
        
    }

    
    @IBAction func addToWalletAction(_ sender: Any) {
        print("Add to wallet")
        if let voucherListItemListModel = self.voucherListVoucherListResponse{
            self.delegate?.addCouponToWishList(voucherItemList: voucherListItemListModel)
        }

    }
       
    @IBAction func viewDetailsBtn(_ sender: Any) {

        if let widgetItemslistwidgetModel = self.widgetItemslistwidgetModel {
            self.delegate?.couponDetailWidget(widgetItemslistwidgetModel: widgetItemslistwidgetModel)
        }

        if let voucherListItemListModel = self.voucherListVoucherListResponse {
            self.delegate?.couponShowDetailList(voucherItemList: voucherListItemListModel)
        }

    }
    
}
