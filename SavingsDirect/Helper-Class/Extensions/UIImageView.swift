//
//  UIImageView.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 11/3/15.
//  Copyright © 2015 Yuji Hato. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    
        
    func loadImage(path:String?) {
        
        if let escapedString = path?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            var aString = escapedString
            let  checkLastString = aString.last
            if checkLastString == " " {
                aString = aString.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
            }
            
            let newString = aString.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil)
            if let url = URL(string:newString){
                self.sd_setImage(with: url as URL, placeholderImage: UIImage(named: DFImageDefault))
                
            } else {
                self.image = UIImage(named:"no-image")
            }
                            
        }  else {
            self.image = UIImage(named:"no-image")
        }
    }
    
    func loadImage(path:String?, placeHolder: String?) {
        
        if let escapedString = path?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            var aString = escapedString
            let  checkLastString = aString.last
            if checkLastString == " " {
                aString = aString.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
            }
            
            let newString = aString.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil)
            if let url = URL(string:newString){
                self.sd_setImage(with: url as URL, placeholderImage: UIImage(named: placeHolder ?? "no-image"))
                
            } else {
                self.image = UIImage(named:placeHolder ?? "no-image")
            }
                            
        }  else {
            self.image = UIImage(named:placeHolder ?? "no-image")
        }
    }
    
    func loadImageFromPath(path:String?, imageName: String?) {
           
           if let escapedString = imageName?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
               var aString = escapedString
               let  checkLastString = aString.last
               if checkLastString == " " {
                   aString = aString.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
               }
               
               let newString = aString.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil)
                if let url = URL(string:((path ?? "") + newString)){
                   self.sd_setImage(with: url as URL, placeholderImage: UIImage(named: DFImageDefault))
                   
               } else {
                   self.image = UIImage(named:"no-image")
               }
                               
           }  else {
               self.image = UIImage(named:"no-image")
           }
       }
    
    
    
    
    func setRandomDownloadImage(_ width: Int, height: Int) {
        if self.image != nil {
            self.alpha = 1
            return
        }
        self.alpha = 0
        let url = URL(string: "http://lorempixel.com/\(width)/\(height)/")!
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 15
        configuration.timeoutIntervalForResource = 15
        configuration.requestCachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        let session = URLSession(configuration: configuration)
        let task = session.dataTask(with: url) { (data, response, error) in
            if error != nil {
                return
            }
            
            if let response = response as? HTTPURLResponse {
                if response.statusCode / 100 != 2 {
                    return
                }
                if let data = data, let image = UIImage(data: data) {
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.image = image
                        UIView.animate(withDuration: 0.3, animations: { () -> Void in
                            self.alpha = 1
                            }, completion: { (finished: Bool) -> Void in
                        }) 
                    })
                }
            }
        }
        task.resume()
    }
    
    func clipParallaxEffect(_ baseImage: UIImage?, screenSize: CGSize, displayHeight: CGFloat) {
        if let baseImage = baseImage {
            if displayHeight < 0 {
                return
            }
            let aspect: CGFloat = screenSize.width / screenSize.height
            let imageSize = baseImage.size
            let imageScale: CGFloat = imageSize.height / screenSize.height
            
            let cropWidth: CGFloat = floor(aspect < 1.0 ? imageSize.width * aspect : imageSize.width)
            let cropHeight: CGFloat = floor(displayHeight * imageScale)
            
            let left: CGFloat = (imageSize.width - cropWidth) / 2
            let top: CGFloat = (imageSize.height - cropHeight) / 2
            
            let trimRect : CGRect = CGRect(x: left, y: top, width: cropWidth, height: cropHeight)
            self.image = baseImage.trim(trimRect: trimRect)
            self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: displayHeight)
        }
    }
    
    func downloadImageFrom(link:String, contentMode: UIView.ContentMode) {
        URLSession.shared.dataTask( with: NSURL(string:link)! as URL, completionHandler: {
            (data, response, error) -> Void in
            DispatchQueue.main.async {
                self.contentMode =  contentMode
                if let data = data { self.image = UIImage(data: data) }
            }
        }).resume()
    }
}
