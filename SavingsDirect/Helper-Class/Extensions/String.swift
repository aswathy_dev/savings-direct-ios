//
//  StringExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/22/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import Foundation

// For MD5 Convertion Values
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG

extension String {
    
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    func substring(_ from: Int) -> String {
        return self.substring(from: self.index(self.startIndex, offsetBy: from))
    }
    
    var length: Int {
        return self.count
    }
    
    var strToInt : Int {
        return Int(self)!
    }
    
    // No More MD5 -> They must use SHA256
    /*
    var md5: String? {
        guard let data = self.data(using: String.Encoding.utf8) else { return nil }
        
        let hash = data.withUnsafeBytes { (bytes: UnsafePointer<Data>) -> [UInt8] in
            var hash: [UInt8] = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
            CC_MD5(bytes, CC_LONG(data.count), &hash)
            return hash
        }
        
        return hash.map { String(format: "%02x", $0) }.joined()
    } */
    
    var integer: Int {
        return Int(self) ?? 0
    }
    
    func currencyFormatterFunc() -> String {
        var myNumber: NSNumber?
        let returnCurrency: String?
        
        let price = self
        if let myInteger = Float(price) {
            myNumber = NSNumber(value:myInteger)
        }

        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
//        formatter.string(from: myNumber ?? NSNumber(0)) // "$123.44"

//        formatter.locale = Locale(identifier: "es_CL")
//        formatter.string(from: price) // $123"

        formatter.locale = Locale(identifier: "es_ES")
        returnCurrency = formatter.string(from: myNumber ?? NSNumber(0)) // "123,44 €"
        
        return returnCurrency ?? ""
         
    }
    
    
    func changeTimeFormatToTwelveHours() -> String {
        
        if self == "" {
            return ""
        }
        
        var notifTime = self
        let dateAsString = notifTime
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let date = dateFormatter.date(from: dateAsString)

        dateFormatter.dateFormat = "h:mm a"
        let date24 = dateFormatter.string(from: date!)
        notifTime = date24
        
        return notifTime
    }
    
    
    var secondFromString : Int{
        let components: Array = self.components(separatedBy: ":")
        let hours = 0//components[0].integer
        let minutes = components[0].integer
        let seconds = components[1].integer
        return Int((hours * 60 * 60) + (minutes * 60) + seconds)
    }
    
    var withoutSpecialCharacters: String {
        return self.components(separatedBy: CharacterSet.symbols).joined(separator: "")
    }
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    var isValidURL : Bool {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        if let match = detector.firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.utf16.count)) {
            // it is a link, if the match covers the whole string
            return match.range.length == self.utf16.count
        } else {
            return false
        }
    }
    
    func index(at position: Int, from start: Index? = nil) -> Index? {
        let startingIndex = start ?? startIndex
        return index(startingIndex, offsetBy: position, limitedBy: endIndex)
    }
    
    func character(at position: Int) -> Character? {
        guard position >= 0, let indexPosition = index(at: position) else {
            return nil
        }
        return self[indexPosition]
    }
    
    
    func isValidEmail() -> Bool {
        var valid: Bool = false
        let emailRegEx = "^(?!.{65})([_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,}))$"
        //  let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        valid = emailTest.evaluate(with: self)
        return valid
    }
    
    func isValidContact() -> Bool {
        if self.count == 10 {
            return true
        }
        return false
    }
	
    func isValidPassword() -> Bool {
        if self.count >= 5 && self.count <= 50 {
            return true
        }
        return false
    }
    
    func trimWhiteSpace() -> String {
        let string = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return string
    }    
    
    func convertDateFormaterWithTime(serverDate : String, returnFormat : String) -> String {
        
        let Nextar = self
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = serverDate
        var dateFromString = Date()
        dateFromString = dateFormatter.date(from: Nextar) ?? Date()
        let lTime: String = dateFormatter.string(from: dateFromString)
        let date: Date? = dateFormatter.date(from: lTime)
        let formatterLbl = DateFormatter()
        formatterLbl.dateFormat = returnFormat
        
        let datelblStr: String = formatterLbl.string(from: date!)
        return datelblStr
        
    }
    
    
    func convertDateFormater(_ date: String, serverDate : String = "yyyy-MM-dd hh:mm:ss" , returnFormat : String) -> String
    {
        if date.contains("T"){
            let Nextar = date.components(separatedBy: "T")
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = serverDate
            var dateFromString = Date()
            dateFromString = dateFormatter.date(from: Nextar[0]) ?? Date()
            let lTime: String = dateFormatter.string(from: dateFromString)
            let date: Date? = dateFormatter.date(from: lTime)
            let formatterLbl = DateFormatter()
            formatterLbl.dateFormat = returnFormat
            formatterLbl.locale = Locale(identifier: "en_US_POSIX")

            let datelblStr: String = formatterLbl.string(from: date!)
            return datelblStr
        }
        else if date.contains(" "){
            let Nextar = date.components(separatedBy: " ")
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"//serverDate
            // var dateFromString = Date()
            let dateFromString = dateFormatter.date(from: Nextar[0]) //?? Date()
            let lTime: String = dateFormatter.string(from: dateFromString!)
            let date: Date? = dateFormatter.date(from: lTime)
            let formatterLbl = DateFormatter()
            formatterLbl.dateFormat = returnFormat
            formatterLbl.locale = Locale(identifier: "en_US_POSIX")

            let datelblStr: String = formatterLbl.string(from: date!)
            return datelblStr
        }
        else{
            return "Never"
        }
        
    }
    
    func isAlphanumeric(ignoreDiacritics: Bool = false) -> Bool {
        if ignoreDiacritics {
            return self.range(of: "[^a-zA-Z ]", options: .regularExpression) == nil && self != ""
        }
        else {
            return self.isAlphanumeric()
        }
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    var localized:String{
        return NSLocalizedString(self, comment: "")
    }
    
    
    
   /* func widthOfString(usingFont font: UIFont) -> CGFloat {
           let fontAttributes = [NSAttributedString.Key.font: font]
           let size = self.size(withAttributes: fontAttributes)
           return size.width
       }

       func heightOfString(usingFont font: UIFont) -> CGFloat {
           let fontAttributes = [NSAttributedString.Key.font: font]
           let size = self.size(withAttributes: fontAttributes)
           return size.height
       }

       func sizeOfString(usingFont font: UIFont) -> CGSize {
           let fontAttributes = [NSAttributedString.Key.font: font]
           return self.size(withAttributes: fontAttributes)
       }
    
    
    func findMinmap(FileType : String) -> String {
        
        let mimeTypes = [
            "xml": "text/xml",
            "gif": "image/gif",
            "jpeg": "image/jpeg",
            "jpg": "image/jpeg",
            "txt": "text/plain",
            "png": "image/png",
            "tif": "image/tiff",
            "tiff": "image/tiff",
            "json": "application/json",
            "doc": "application/msword",
            "pdf": "application/pdf"
        ]
        return mimeTypes[FileType] ?? "image/jpeg"
    }
        
    func htmlTextToStringValue(htmlString: String, fontData: UIFont, colorData: UIColor) -> NSMutableAttributedString {
        
        let attributedString1 = NSMutableAttributedString()
        let data = htmlString.data(using: String.Encoding.unicode)!
        
        let attributedString2 = try? NSMutableAttributedString(
                                  data: data,
                                  options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                                  documentAttributes: nil)
        
        var attributedStringTemp = NSMutableAttributedString()
        attributedStringTemp = attributedString2 ?? NSMutableAttributedString(attributedString: (NSAttributedString(string: "")))
        attributedStringTemp.enumerateAttribute(
          NSAttributedString.Key.font,
          in:NSMakeRange(0,attributedStringTemp.length),
          options:.longestEffectiveRangeNotRequired) { value, range, stop in
            let f1 = fontData
            let f2 = fontData
            if let f3 = applyTraitsFromFont(f1, to:f2) {
                attributedStringTemp.addAttribute(
                    NSAttributedString.Key.font, value:f3, range:range)
                attributedStringTemp.addAttribute(NSAttributedString.Key.foregroundColor, value:colorData, range: range)
            }
        }
            
        attributedString1.append(attributedStringTemp)
        
        return attributedString1
        
    }
    
    func applyTraitsFromFont(_ f1: UIFont, to f2: UIFont) -> UIFont? {
          let t = f1.fontDescriptor.symbolicTraits
          if let fd = f2.fontDescriptor.withSymbolicTraits(t) {
              return UIFont.init(descriptor: fd, size: 0)
          }
          return nil
    }*/
    
}


extension Int {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}


extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

/*
 
 Wednesday, Sep 12, 2018           --> EEEE, MMM d, yyyy
 09/12/2018                        --> MM/dd/yyyy
 09-12-2018 14:11                  --> MM-dd-yyyy HH:mm
 Sep 12, 2:11 PM                   --> MMM d, h:mm a
 September 2018                    --> MMMM yyyy
 Sep 12, 2018                      --> MMM d, yyyy
 Wed, 12 Sep 2018 14:11:54 +0000   --> E, d MMM yyyy HH:mm:ss Z
 2018-09-12T14:11:54+0000          --> yyyy-MM-dd'T'HH:mm:ssZ
 12.09.18                          --> dd.MM.yy
 10:41:02.112                      --> HH:mm:ss.SSS
 
*/

