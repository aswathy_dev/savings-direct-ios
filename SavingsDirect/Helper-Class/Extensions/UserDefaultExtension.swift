//
//  LoginModel.swift
//  Olvo
//
//  Created by Preesa  on 28/02/19.
//  Copyright © 2019 Codcor Technologies. All rights reserved.
//

import Foundation

extension UserDefaults{
    
    //MARK: Check Login
    func setLoggedIn(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isLoggedIn.rawValue)
        //synchronize()
    }
    
    func isLoggedIn()-> Bool {
        return bool(forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    
    //MARK: Save User Data
    func setUserID(value: String){
        set(value, forKey: UserDefaultsKeys.userID.rawValue)
        //synchronize()
    }
    
    //MARK: Retrieve User Data
    func getUserID() -> String{
        //return string(forKey: UserDefaultsKeys.userID.rawValue)!
        return string(forKey: UserDefaultsKeys.userID.rawValue) ?? ""
    }
    
    func setLanguageID(value : String){
        set(value, forKey: UserDefaultsKeys.language.rawValue)
    }
    
    func getLanguageID() -> String{
        return string(forKey: UserDefaultsKeys.language.rawValue)!
    }
    
    func setDeviceID(value : String){
        set(value, forKey: UserDefaultsKeys.deviceID.rawValue)
    }
    
    func getDeviceID() -> String{
        return string(forKey: UserDefaultsKeys.deviceID.rawValue) ?? ""
    }
    
    enum UserDefaultsKeys : String {
        case isLoggedIn
        case userID
        case language
        case deviceID
    }
    
    func removeAll(){
        
        let DeveiceID = self.getDeviceID()
        let CityID = Session.getCityID()
        let CityName = Session.getCityName()
        let selectedLang = UserDefaults.standard.value(forKey: "LCLCurrentLanguageKey")

        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        
        self.setDeviceID(value: DeveiceID)
        UserDefaults.standard.set(CityID, forKey: "CityID")
        UserDefaults.standard.set(CityName, forKey: "CityName")
        UserDefaults.standard.set(selectedLang, forKey: "LCLCurrentLanguageKey")

        UserDefaults.standard.synchronize()

    }
}

