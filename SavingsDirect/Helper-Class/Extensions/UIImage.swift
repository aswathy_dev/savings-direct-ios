//
//  UIIMage.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 11/5/15.
//  Copyright © 2015 Yuji Hato. All rights reserved.
//

import UIKit

enum JPEGQuality: CGFloat {
       case lowest  = 0
       case low     = 0.25
       case medium  = 0.5
       case high    = 0.75
       case highest = 1
   }

extension UIImage {
    func trim(trimRect :CGRect) -> UIImage {
        if CGRect(origin: CGPoint.zero, size: self.size).contains(trimRect) {
            if let imageRef = self.cgImage?.cropping(to: trimRect) {
                return UIImage(cgImage: imageRef)
            }
        }
        
        UIGraphicsBeginImageContextWithOptions(trimRect.size, true, self.scale)
        self.draw(in: CGRect(x: -trimRect.minX, y: -trimRect.minY, width: self.size.width, height: self.size.height))
        let trimmedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let image = trimmedImage else { return self }
        
        return image
    }
    
    func imageWithColor(color1: UIColor) -> UIImage {
           UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
           color1.setFill()

           let context = UIGraphicsGetCurrentContext()
           context?.translateBy(x: 0, y: self.size.height)
           context?.scaleBy(x: 1.0, y: -1.0)
           context?.setBlendMode(CGBlendMode.normal)

           let rect = CGRect(origin: .zero, size: CGSize(width: self.size.width, height: self.size.height))
           context?.clip(to: rect, mask: self.cgImage!)
           context?.fill(rect)

           let newImage = UIGraphicsGetImageFromCurrentImageContext()
           UIGraphicsEndImageContext()

           return newImage!
       }
    

       /// Returns the data for the specified image in JPEG format.
       /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
       /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
       func jpeg(_ quality: JPEGQuality) -> Data? {
        return self.jpegData(compressionQuality: quality.rawValue)
       }
}
