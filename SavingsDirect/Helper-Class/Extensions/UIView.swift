//
//  UIView.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 11/5/15.
//  Copyright © 2015 Yuji Hato. All rights reserved.
//

import UIKit
import MapKit
import Foundation

//@IBDesignable
//class DesignableView: UIView {
//}
//
//@IBDesignable
//class DesignableButton: UIButton {
//}

//@IBDesignable
//class DesignableLabel: UILabel {
//}

enum LINE_POSITION {
    case LINE_POSITION_TOP
    case LINE_POSITION_BOTTOM
}

extension UIView {
  
//  @IBInspectable
//  var cornerRadius: CGFloat {
//    get {
//      return layer.cornerRadius
//    }
//    set {
//      layer.cornerRadius = newValue
//    }
//  }
////
//  @IBInspectable
//  var borderWidth: CGFloat {
//    get {
//      return layer.borderWidth
//    }
//    set {
//      layer.borderWidth = newValue
//    }
//  }
//
//  @IBInspectable
//  var borderColor: UIColor? {
//    get {
//      if let color = layer.borderColor {
//        return UIColor(cgColor: color)
//      }
//      return nil
//    }
//    set {
//      if let color = newValue {
//        layer.borderColor = color.cgColor
//      } else {
//        layer.borderColor = nil
//      }
//    }
//  }
    
    
//
//  @IBInspectable
//  var shadowRadius: CGFloat {
//    get {
//      return layer.shadowRadius
//    }
//    set {
//      layer.shadowRadius = newValue
//    }
//  }
//
//  @IBInspectable
//  var shadowOpacity: Float {
//    get {
//      return layer.shadowOpacity
//    }
//    set {
//      layer.shadowOpacity = newValue
//    }
//  }
//
//  @IBInspectable
//  var shadowOffset: CGSize {
//    get {
//      return layer.shadowOffset
//    }
//    set {
//      layer.shadowOffset = newValue
//    }
//  }
//
//  @IBInspectable
//  var shadowColor: UIColor? {
//    get {
//      if let color = layer.shadowColor {
//        return UIColor(cgColor: color)
//      }
//      return nil
//    }
//    set {
//      if let color = newValue {
//        layer.shadowColor = color.cgColor
//      } else {
//        layer.shadowColor = nil
//      }
//    }
//  }

        func rotate() {
            let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
            rotation.toValue = NSNumber(value: Double.pi * 2)
            rotation.duration = 2
            rotation.isCumulative = true
            rotation.repeatCount = Float.greatestFiniteMagnitude
            self.layer.add(rotation, forKey: "rotationAnimation")
        }
        
        func setBaseViewViewDropShadow() {
      
    //       self.backgroundColor = UIColor.clear
    //       self.layer.shadowColor = UIColor.black.cgColor
    //       self.layer.shadowOffset = CGSize(width: 20, height: 10)
    //        self.layer.shadowOpacity = 0.7
    //       self.layer.shadowRadius = 4.0
    //        self.layer.borderColor = UIColor.clear.cgColor
    //
    //
    //        let borderView = UIView()
    //        borderView.frame = self.bounds
    //        borderView.layer.cornerRadius = 10
    //        borderView.layer.borderColor = UIColor.clear.cgColor
    //        borderView.layer.borderWidth = 1.0
    //        borderView.layer.masksToBounds = false
    //        self.addSubview(borderView)
            
    //        self.layer.cornerRadius = 10.0
    //       self.layer.borderWidth = 1.0
    //       self.layer.borderColor = UIColor.clear.cgColor
    //       //self.layer.masksToBounds = true
    //       self.layer.shadowColor = UIColor.lightGray.cgColor
    //       self.layer.shadowOffset = CGSize(width: 3, height: 3)
    //        self.layer.shadowRadius = 14.0
    //       self.layer.shadowOpacity = 0.5
    //       self.layer.masksToBounds = true
    //       self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
        }
    
    class func loadNib<T: UIView>(_ viewType: T.Type) -> T {
          let className = String.className(viewType)
          return Bundle(for: viewType).loadNibNamed(className, owner: nil, options: nil)!.first as! T
    }
      
    class func loadNib() -> Self {
          return loadNib(self)
    }
    
    func viewWithborder()
    {
        layer.borderColor = UIColor(red: 185/255, green: 185/255, blue: 185/255, alpha: 1).cgColor
        layer.borderWidth = 1
        layer.cornerRadius = 5
        layer.masksToBounds = false
    }
    
    func addLine(position : LINE_POSITION, color: UIColor, width: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false // This is important!
        self.addSubview(lineView)

        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))

        switch position {
        case .LINE_POSITION_TOP:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .LINE_POSITION_BOTTOM:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        }
    }
    func addShadow(_ opacity:Float,radius:CGFloat = 1.0,size:CGSize=CGSize(width: 1, height: 1))  {
        self.layer.masksToBounds = false
        self.layer.shadowRadius = radius
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = size
        self.layer.shadowOpacity = opacity
    }
    
    func addBorder(_ color:UIColor = .lightGray,width:CGFloat = 1) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
    
    func addRoundedCorners() {
        self.layer.cornerRadius = self.bounds.height/2
        
    }
    
    func asImage() -> UIImage {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(bounds: bounds)
            return renderer.image { rendererContext in
                layer.render(in: rendererContext.cgContext)
            }
        } else {
            UIGraphicsBeginImageContext(self.frame.size)
            self.layer.render(in:UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return UIImage(cgImage: image!.cgImage!)
        }
    }
    
    func exportAsPdfFromView(fileName:String) -> String {
        
        let pdfPageFrame = self.bounds
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageFrame, nil)
        UIGraphicsBeginPDFPageWithInfo(pdfPageFrame, nil)
        guard let pdfContext = UIGraphicsGetCurrentContext() else { return "" }
        self.layer.render(in: pdfContext)
        UIGraphicsEndPDFContext()
        return saveViewPdf(data: pdfData, fileName: fileName)
        
    }
    
    func saveViewPdf(data: NSMutableData, fileName:String) -> String {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docDirectoryPath = paths[0]
        let pdfPath = docDirectoryPath.appendingPathComponent("\(fileName).pdf")
        if data.write(to: pdfPath, atomically: true) {
            return pdfPath.path
        } else {
            return ""
        }
    }
    
//    func addshadow(top: Bool,
//                   left: Bool,
//                   bottom: Bool,
//                   right: Bool,
//                   shadowRadius: CGFloat = 2.0) {
//
//        self.layer.masksToBounds = false
//        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
//        self.layer.shadowRadius = shadowRadius
//        self.layer.shadowOpacity = 1.0
//
//        let path = UIBezierPath()
//        var x: CGFloat = 0
//        var y: CGFloat = 0
//        var viewWidth = self.frame.width
//        var viewHeight = self.frame.height
//
//        // here x, y, viewWidth, and viewHeight can be changed in
//        // order to play around with the shadow paths.
//        if (!top) {
//            y+=(shadowRadius+1)
//        }
//        if (!bottom) {
//            viewHeight-=(shadowRadius+1)
//        }
//        if (!left) {
//            x+=(shadowRadius+1)
//        }
//        if (!right) {
//            viewWidth-=(shadowRadius+1)
//        }
//        // selecting top most point
//        path.move(to: CGPoint(x: x, y: y))
//        // Move to the Bottom Left Corner, this will cover left edges
//        /*
//         |☐
//         */
//        path.addLine(to: CGPoint(x: x, y: viewHeight))
//        // Move to the Bottom Right Corner, this will cover bottom edge
//        /*
//         ☐
//         -
//         */
//        path.addLine(to: CGPoint(x: viewWidth, y: viewHeight))
//        // Move to the Top Right Corner, this will cover right edge
//        /*
//         ☐|
//         */
//        path.addLine(to: CGPoint(x: viewWidth, y: y))
//        // Move back to the initial point, this will cover the top edge
//        /*
//         _
//         ☐
//         */
//        path.close()
//        self.layer.shadowPath = path.cgPath
//    }
}


extension UIScrollView {

    // Scroll to a specific view so that it's top is at the top our scrollview
    func scrollToView(view:UIView, animated: Bool) {
        if let origin = view.superview {
            // Get the Y position of your child view
            let childStartPoint = origin.convert(view.frame.origin, to: self)
            // Scroll to a rectangle starting at the Y of your subview, with a height of the scrollview
            self.scrollRectToVisible(CGRect(x:0, y:childStartPoint.y,width: 1,height: self.frame.height), animated: animated)
        }
    }

    // Bonus: Scroll to top
    func scrollToTop(animated: Bool) {
        let topOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(topOffset, animated: animated)
    }

    // Bonus: Scroll to bottom
    func scrollToBottom() {
        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom)
        if(bottomOffset.y > 0) {
            setContentOffset(bottomOffset, animated: true)
        }
    }

}


extension MKMapView {

    func topCenterCoordinate() -> CLLocationCoordinate2D {
        return self.convert(CGPoint(x: self.frame.size.width / 2.0, y: 0), toCoordinateFrom: self)
    }

    func currentRadius() -> Double {
        let centerLocation = CLLocation(latitude: self.centerCoordinate.latitude, longitude: self.centerCoordinate.longitude)
        let topCenterCoordinate = self.topCenterCoordinate()
        let topCenterLocation = CLLocation(latitude: topCenterCoordinate.latitude, longitude: topCenterCoordinate.longitude)
        return centerLocation.distance(from: topCenterLocation)
    }

}
