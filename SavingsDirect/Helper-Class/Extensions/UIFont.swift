//
//  UIFont.swift
//  Oorlit
//
//  Created by ispg on 16/10/19.
//  Copyright © 2019 ISPG technologies india pvt ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    public enum Gilroy : String {
        
        case black      = "-Black"
        case bold       = "-Bold"
        case extraBold  = "-ExtraBold"
        case heavy      = "-Heavy"
        case light      = "-Light"
        case medium     = "-Medium"
        case regular    = "-Regular"
        case semibold   = "-Semibold"
        case thin       = "-Thin"
        case ultraLight = "-UltraLight"
    }
    
    static func Font(_ type: Gilroy, size: CGFloat = UIFont.systemFontSize) -> UIFont {
         
        let fontName = "Gilroy" + type.rawValue as String
        return UIFont(name: fontName, size: size)!
    }
    
    var isBold: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitBold)
    }
    
    var isItalic: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitItalic)
    }
    
    
}
