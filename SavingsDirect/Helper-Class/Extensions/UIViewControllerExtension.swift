//
//  UIViewControllerExtension.swift
//  Olvo
//
//  Created by Preesa  on 13/03/19.
//  Copyright © 2019 Codcor Technologies. All rights reserved.
//

import Foundation
import UIKit
import Toast_Swift

enum DeviceTypeModel{
    case iphoneX
    case iphone8Plus
    case iphone8
    case iphoneSE //SE is the like iphone 5 and iphone 5s
    case iphone4s
    case ipadPro12
}

extension UITabBar{
    func inActiveTintColor() {
        if let items = items{
            for item in items{
                item.image =  item.image?.withRenderingMode(.alwaysOriginal)
                item.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.lightGray], for: .normal)
                item.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], for: .selected)
            }
        }
    }
}

var vSpinner : UIView?
var vInner : UIView?

extension UIViewController {
    
    func transparentNavBg() {
        UINavigationBar.appearance().shadowImage = UIImage()
//        UINavigationBar.appearance().addShadow(0, radius: 0, size: CGSize(width: 0,height: 0))
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        UINavigationBar.appearance().isTranslucent = false

//
//        let navigationBar = navigationController?.navigationBar
//        let navigationBarAppearence = UINavigationBarAppearance()
//        navigationBarAppearence.shadowColor = .clear
//        navigationBar?.scrollEdgeAppearance = navigationBarAppearence

    }
    
    func setNavigationBarBg() {
        let shadowView = UIView(frame: self.navigationController!.navigationBar.frame)
        shadowView.backgroundColor = UIColor.white
        shadowView.layer.shadowColor = UIColor.lightGray.cgColor //###
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowOpacity = 1//0.4 //0.1
        shadowView.layer.shadowOffset = CGSize(width: 2, height: 2) //0 4
        shadowView.layer.shadowRadius =  3// 1
        self.view.addSubview(shadowView)
        //self.navigationController?.navigationBar.setBackgroundImage(UIImage.init(named: "bgDropshadow"),
        //        for: .default)
    }
    
    func presentDetail(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.4
        transition.type = CATransitionType.push
        transition.subtype =  CATransitionSubtype.fromLeft
        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        present(viewControllerToPresent, animated: false)
    }
    
    func dismissDetail() {
        let transition = CATransition()
        transition.duration = 0.4
        transition.type = CATransitionType.push
        transition.subtype =  CATransitionSubtype.fromRight
        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: false)
    }
    
    
    func pushDetail(_ viewControllerToPresent: UIViewController) {
       let transition = CATransition()
       transition.duration = 0.5
       transition.type = CATransitionType.push
       transition.subtype =  CATransitionSubtype.fromRight
       self.view.window!.layer.add(transition, forKey: kCATransition)
       self.navigationController?.pushViewController(viewControllerToPresent, animated: false)
    }
    
    func popDetail() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype =  CATransitionSubtype.fromLeft
        self.view.window!.layer.add(transition, forKey: kCATransition)
        self.navigationController?.popViewController(animated: false)
    }
    
    
    func loadNavigationItemHead() {
        let vwNavigationTitle = UIView()
        vwNavigationTitle.frame = CGRect(x: 0.0, y: 0.0, width: 150, height: 42)
        let logo = UIImage(named: "logo.png")
        let imageView = UIImageView(image:logo)
        imageView.frame = CGRect(x: 0.0, y: 0.0, width: 150, height: 42)
        imageView.center = vwNavigationTitle.center
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        vwNavigationTitle.addSubview(imageView)
        self.navigationItem.titleView = vwNavigationTitle
        //imageView.center = self.navigationItem.titleView!.center
        // self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "navBg"), for: UIBarMetrics.default)
        // self.navigationController?.navigationBar.shadowImage = UIImage(named: "navBg")
    }
    
    func showHideNavigationBarHead(bHidden: Bool) {
        self.navigationController?.setNavigationBarHidden(bHidden, animated: false)
    }
    
    
    func showHideTabbarViewBottom(bHidden: Bool) {
        self.tabBarController?.tabBar.isHidden = bHidden
    }
    
    func switchTabBar(index:Int) {
//        CommonClass.changeTabbarIndex(index: index)
       // self.tabBarController?.selectedIndex = index
    }
    
    func add(_ child: UIViewController, onView: UIView? = nil) {
        var holderView = self.view!
    
        if let onView = onView{
            holderView = onView
        }
        
        addChild(child)
        child.view.frame = holderView.bounds
        holderView.addSubview(child.view)
        child.didMove(toParent: self)
    }
    
    func showToastFaded(message : String) {


        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 125, y: self.view.frame.size.height-100, width: 250, height: 35))
        toastLabel.numberOfLines = 0
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        toastLabel.sizeToFit()
        toastLabel.frame = CGRect( x: toastLabel.frame.minX, y: toastLabel.frame.minY,width:   toastLabel.frame.width + 20, height: toastLabel.frame.height + 8)

        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func showAlert(title: String, msg: String) {
        
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        alert.view.tintColor = UIColor.black
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertDismiss(title: String, msg: String) {
        
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) { action in
            self.dismiss(animated: true)
        }
        alert.addAction(action)
        alert.view.tintColor = UIColor.black
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertPopControl(title: String, msg: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) { action in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(action)
        alert.view.tintColor = UIColor.black
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func runOnTheDeviceType(_ completion: (DeviceTypeModel) -> Void) {
        if UIDevice().userInterfaceIdiom == .phone{
            switch UIScreen.main.nativeBounds.height{
            case 2436:
                completion(.iphoneX)
            case 1920:
                completion(.iphone8Plus)
            case 1334:
                completion(.iphone8)
            case 1136:
                completion(.iphoneSE)
            case 1366:
                completion(.ipadPro12)
            default:
                completion(.iphone4s)
            }
        }
    }
         
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(hex: "#FEF9E7")// init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        spinnerView.alpha = 0.2
        
        let innerView = UIView.init(frame: onView.bounds)
        innerView.backgroundColor = UIColor.clear
        innerView.alpha = 1

        let imageView = UIImageView()
        imageView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        imageView.center = spinnerView.center
        imageView.image = UIImage.init(named: "symb_activity")
        imageView.contentMode = .scaleAspectFit
//        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
//        ai.startAnimating()
//        ai.center = spinnerView.center
        
       // DispatchQueue.main.async {
            innerView.addSubview(imageView)
            onView.addSubview(spinnerView)
            onView.addSubview(innerView)

            imageView.rotate()
       // }
        
        vSpinner = spinnerView
        vInner = innerView
    }
    
    func removeSpinner() {
        //DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
            
            vInner?.removeFromSuperview()
            vInner = nil
       // }
    }
    
    func showSuccessMessage(title: String, message: String, buttonText: String, completion: @escaping (String) -> ()) {
           let alert = UIAlertController(title: title,
                                         message: message,
                                         preferredStyle: UIAlertController.Style.alert)
           alert.addAction(UIAlertAction(title: buttonText,
                                         style: UIAlertAction.Style.cancel,
                                         handler: { _ in
                                           
                                           completion("true")
           }))
           self.present(alert, animated: true, completion: nil)
    }
    
    func showToastMessage(message: String) {
    
        var style = ToastStyle()
        style.backgroundColor = UIColor.COLOR_SD_BLUE_L
        style.messageColor = .white
        self.view.makeToast(message, duration: 2.0, position: .bottom, style: style)
    }
    
    func shareAction(shareContent: String, shareURL: String, completion: @escaping (String) -> ()) {
        let someText:String = shareContent
        let urls = shareURL
        let objectsToShare:URL = URL(string: urls)!
        let sharedObjects:[AnyObject] = [objectsToShare as AnyObject,someText as AnyObject]
        let activityViewController = UIActivityViewController(activityItems : sharedObjects, applicationActivities: nil)
//        activityViewController.delete(self)
        activityViewController.setValue(someText, forKey: "Subject")
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToTencentWeibo,
            UIActivity.ActivityType.openInIBooks,
            UIActivity.ActivityType.airDrop,
            UIActivity.ActivityType(rawValue: "com.apple.reminders.RemindersEditorExtension"),
            UIActivity.ActivityType(rawValue: "com.apple.mobilenotes.SharingExtension"),
            UIActivity.ActivityType(rawValue: "com.google.Drive.ShareExtension"),
            UIActivity.ActivityType(rawValue: "com.apple.mobileslideshow.StreamShareService"),
//            UIActivity.ActivityType(rawValue: "net.whatsapp.WhatsApp.ShareExtension"),
            UIActivity.ActivityType(rawValue: "pinterest.ShareExtension"),
//            UIActivity.ActivityType(rawValue: "com.facebook.Messenger.ShareExtension"),
            UIActivity.ActivityType(rawValue: "com.tinyspeck.chatlyio.share"), // Slack!
            UIActivity.ActivityType(rawValue: "ph.telegra.Telegraph.Share"),
            UIActivity.ActivityType(rawValue: "com.google.Drive.ShareExtension"),
            UIActivity.ActivityType(rawValue: "com.toyopagroup.picaboo.share"), // Snapchat!
            UIActivity.ActivityType(rawValue: "wefwef.YammerShare"),
            UIActivity.ActivityType(rawValue: "com.fogcreek.trello.trelloshare"),
//            UIActivity.ActivityType(rawValue: "com.linkedin.LinkedIn.ShareExtension"),
            UIActivity.ActivityType(rawValue: "com.hammerandchisel.discord.Share"),
//            UIActivity.ActivityType(rawValue: "com.google.Gmail.ShareExtension"),
            UIActivity.ActivityType(rawValue: "com.google.inbox.ShareExtension"),
            UIActivity.ActivityType(rawValue: "com.riffsy.RiffsyKeyboard.RiffsyShareExtension"), //GIF Keyboard by Tenor
            UIActivity.ActivityType(rawValue: "com.google.hangouts.ShareExtension"),
            UIActivity.ActivityType(rawValue: "com.ifttt.ifttt.share"),
            UIActivity.ActivityType(rawValue: "com.amazon.Lassen.SendToKindleExtension"),
            UIActivity.ActivityType(rawValue: "com.google.chrome.ios.ShareExtension"),
            UIActivity.ActivityType(rawValue: "com.skype.skype.sharingextension")

        ]
        
        self.present(activityViewController, animated: true, completion: nil)
        
        activityViewController.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
                 if completed {
                      
                 } else {
                      completion("false")
                      return
                  }
                  
                  if (activityType == nil)    {
                      completion("false")
                      return
                  }
                  
                  if error == nil {
                      completion("true")
                      
                  } else {
                      completion("false")
                  }
              }
    }
    
    
//    func setCartTabBadge(index: String) {
//        //self.navigationController?.tabBarItem.badgeValue = index
//        /*let cart: UITabBarItem = self.tabBarController?.viewControllers?[1].tabBarItem ?? UITabBarItem()
//        cart.badgeValue = index*/
//        
//        
//        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        let vc: UBMainTabbarVC = mainStoryboard.instantiateViewController(withIdentifier: "tabbarVC") as! UBMainTabbarVC
//
//        if let tabItems = vc.tabBar.items {
//            let tabItem = tabItems[1]
//            tabItem.badgeValue = index
//        }
//    }
    
        
}

extension UIApplication {
    
    class var topViewController: UIViewController? {
        return getTopViewController()
    }
    
    private class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return getTopViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}

extension Equatable {
    func share() {
        let activity = UIActivityViewController(activityItems: [self], applicationActivities: nil)
        UIApplication.topViewController?.present(activity, animated: true, completion: nil)
    }
}

