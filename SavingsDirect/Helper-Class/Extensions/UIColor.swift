//
//  UIColor.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 11/5/15.
//  Copyright © 2015 Yuji Hato. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let COLOR_SD_BLACK = UIColor.init(hex: "#363636")
    static let COLOR_SD_BLUE_L = UIColor.init(hex: "#5CA8DA")
    static let COLOR_SD_GRAY_L = UIColor.init(hex: "#BEBEBE")
    static let COLOR_SD_GRAY_N = UIColor.init(hex: "#585858")

    
    
   /* static let COLOR_SLOT_PINK = UIColor.init(hex: "#E95E96")
    static let COLOR_SLOT_GREEN = UIColor.init(hex: "#8BFBC0")
    static let COLOR_SLOT_ORANGE = UIColor.init(hex: "#EA6C36")
    static let COLOR_SLOT_YELLOW = UIColor.init(hex: "#F6CD64")
    static let COLOR_SLOT_PURPLE = UIColor.init(hex: "#5015A8")
    static let COLOR_SLOT_DPURPLE = UIColor.init(hex: "#3e2f54")
    static let COLOR_SLOT_BGGray = UIColor.init(hex: "#F5F7F9")
    static let COLOR_BUTTON_BG = UIColor.init(hex: "#E95E96")
    

    static let COLOR_GREEN_L = UIColor.init(hex: "#11b7a1")
    static let COLOR_GRAY_L = UIColor.init(hex:"#90a4ae")
    static let COLOR_DARK_BLACK = UIColor.init(hex:"#191919")
    static let COLOR_GRAY = UIColor.init(hex:"#828181")
    static let COLOR_ORANGE = UIColor.init(hex:"#ffa000")
    static let COLOR_RED = UIColor.init(hex:"#ff290d")
    static let COLOR_NORATING = UIColor.init(hex:"#94A3AD")
    static let COLOR_BLUE = UIColor.init(hex:"#3a51f5")
    static let COLOR_SANDAL = UIColor.init(hex:"#FCF1DB")
    static let COLOR_CONTINUE = UIColor.init(hex: "#EFF1FF")

     static let COLOR_PRIME = UIColor(red: 73/255, green: 97/255, blue: 193/255, alpha: 1.0)
     static let COLOR_ORANGE_D = UIColor(red: 222/255, green: 82/255, blue: 65/255, alpha: 1.0)
     static let COLOR_GRAY_D = UIColor(red: 74/255, green: 74/255, blue: 87/255, alpha: 1.0)
     static let COLOR_GRAY_THIN = UIColor(red: 242/255, green:242/255, blue: 247/255, alpha: 1.0)
     static let COLOR_ORANGE_L = UIColor(red: 230/255, green: 150/255, blue: 73/255, alpha: 1.0)
     static let COLOR_PURPLE_L = UIColor(red: 89/255, green: 59/255, blue: 171/255, alpha: 1.0)
     static let COLOR_BLUE_L = UIColor(red: 74/255, green: 94/255, blue: 241/255, alpha: 1.0)
     static let COLOR_RED_D = UIColor(red: 171/255, green: 64/255, blue: 52/255, alpha: 1.0)
     static let COLOR_YELLOW_L = UIColor(red: 252/255, green: 245/255, blue: 230/255, alpha: 1.0)
     static let COLOR_RED_L = UIColor(red: 250/255, green: 237/255, blue: 234/255, alpha: 1.0)
     static let COLOR_FAV_ADDED = UIColor(red: 223/255, green: 245/255, blue: 242/255, alpha: 1.0)
     static let COLOR_FAV_NORMAL = UIColor(red: 252/255, green: 241/255, blue: 220/255, alpha: 1.0)
//     static let COLOR_BUTTON_BG = UIColor(red: 238/255, green: 154/255, blue: 69/255, alpha: 1.0)*/

     
    convenience init(hex: String) {
        self.init(hex: hex, alpha:1)
    }

    convenience init(hex: String, alpha: CGFloat) {
        var hexWithoutSymbol = hex
        if hexWithoutSymbol.hasPrefix("#") {
            hexWithoutSymbol = hex.substring(1)
        }
        
        let scanner = Scanner(string: hexWithoutSymbol)
        var hexInt:UInt32 = 0x0
        scanner.scanHexInt32(&hexInt)
        
        var r:UInt32!, g:UInt32!, b:UInt32!
        switch (hexWithoutSymbol.length) {
        case 3: // #RGB
            r = ((hexInt >> 4) & 0xf0 | (hexInt >> 8) & 0x0f)
            g = ((hexInt >> 0) & 0xf0 | (hexInt >> 4) & 0x0f)
            b = ((hexInt << 4) & 0xf0 | hexInt & 0x0f)
            break;
        case 6: // #RRGGBB
            r = (hexInt >> 16) & 0xff
            g = (hexInt >> 8) & 0xff
            b = hexInt & 0xff
            break;
        default:
            // TODO:ERROR
            break;
        }
        
        self.init( red: (CGFloat(r)/255), green: (CGFloat(g)/255), blue: (CGFloat(b)/255), alpha:alpha)
    }
}
