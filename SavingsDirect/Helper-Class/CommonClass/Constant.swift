//
//  Constant.swift
//  Oorlit
//
//  Created by ispg on 16/10/19.
//  Copyright © 2019 ISPG technologies india pvt ltd. All rights reserved.
//

import Foundation
import UIKit

let ScreenSize  = UIScreen.main.bounds
let ScreenWidth = ScreenSize.width
let ScreenHight = ScreenSize.height

let GLOB_FONT         = UIFont(name: "Roboto-Regular",   size: FONT_HIGHT)
let GLOB_FONT_BOLD    = UIFont(name: "Roboto-Bold",      size: FONT_HIGHT)
let GLOB_FONT_SEMI    = UIFont(name: "Roboto-Semibold",  size: FONT_HIGHT)

var FONT_HIGHT              : CGFloat { return 15.0 }
var FONT_HIGHT_TITLE        : CGFloat { return 22.0 }
var FONT_HIGHT_DESCRIPTION  : CGFloat { return 14.0 }

let PHImage = "no-image"
let PHImageURL = "https://stg70.oorjit.net/ummabee.com/public/uploads/catalog/product/large/u/n/Untitled-1__1877736463.jpg"
let PHImageBaseURL = "https://stg70.oorjit.net/ummabee.com/public/uploads/catalog/product/thumb/"

let BACK_ARROW_WIDTH = 20.0
let BACK_ARROW_HEIGHT = 25.0

let WIDGET_DEAL_CELL_HEIGHT = 400//410
let WIDGET_DEAL_ITEM_HEIGHT = 300
let WIDGET_DEAL_ITEM_WIDTH = 260

let WIDGET_DEAL_SUGGESTED_CELL_HEIGHT = 480
let WIDGET_DEAL_SUGGESTED_ITEM_HEIGHT = 340
let WIDGET_DEAL_SUGGESTED_ITEM_WIDTH = 260

let WIDGET_COUPON_CELL_HEIGHT = 330//320
let WIDGET_COUPON_ITEM_HEIGHT = 255
let WIDGET_COUPON_ITEM_WIDTH = 200

let WIDGET_STATIC_WIDGET_HEIGHT = 260

let APP_COMMON_BUTTON_HEIGHT = 45
let APP_COMMON_BUTTON_RADIUS = 5
