//
//  CommonClass.swift
//  FloKoin
//
//  Created by KG Sajith on 07/11/19.
//  Copyright © 2019 ISPG. All rights reserved.
//

import UIKit
import MapKit

protocol alertDelegate {
  func okAction()
}

class CommonClass: NSObject {
    
    static let shared = CommonClass()
    var delegate: alertDelegate?

        static func showCustomAlertView(title: String, vc: UIViewController) {
            let storyBrd = UIStoryboard(name: "CommonUseBoard", bundle: nil)
            let alertViewCustom =  storyBrd.instantiateViewController(withIdentifier:"CommonAlertView") as! UIViewController
            alertViewCustom.modalPresentationStyle = .fullScreen
            vc.present(alertViewCustom, animated: true, completion: nil)
        }
    
  
         static func openWebsite(strWebsite: String) {
           let url = "\(strWebsite)"
           if UIApplication.shared.canOpenURL(NSURL(string: url)! as URL) {
               if #available(iOS 10.0, *) {
                   UIApplication.shared.open(NSURL(string: url)! as URL, options: [:], completionHandler: { (success) in
                       
                   })
               } else {
                   UIApplication.shared.openURL(NSURL(string: url)! as URL)
               }
           }
         }
         
         static func convertDateFormat(date: String, currentFormat: String, desiredFormat: String) -> String{
             let dateFormatter = DateFormatter()
             let dateStr = date
             dateFormatter.dateFormat = currentFormat
             let date1 = dateFormatter.date(from: dateStr)
             dateFormatter.dateFormat = desiredFormat
             let currentDateString: String = dateFormatter.string(from: date1!)
             return currentDateString
         }
         
     //    static func returnAmenityImage(amenityItem: String) -> String{
     //        if amenityItem == "Gym" {
     //            return "Gym"
     //
     //        } else if amenityItem == "Free Wifi" {
     //            return "wifi"
     //
     //        } else if amenityItem == "Kids Play Area" {
     //            return "kids_play_area"
     //
     //        } else if amenityItem == "Garden" {
     //            return "place_holder"
     //
     //        } else if amenityItem == "Parking" {
     //            return "place_holder"
     //
     //        }
     //        return "place_holder"
     //    }
    
    
    static func returnWeekDay(weekDay: String) -> String {
        
        switch weekDay {
            
            case "Monday":
                return "MONDAY"//"Mon"
                
            case "Tuesday":
                return "TUESDAY"//"Tue"
                
            case "Wednesday":
                return "WEDNESDAY"//"Wed"
                
            case "Thursday":
                return "THURSDAY"//"Thu"
                
            case "Friday":
                return "FRIDAY"//"Fri"
                
            case "Saturday":
                return "SATURDAY" //"Sat"
                
            case "Sunday":
                return "SUNDAY"//"Sun"
                
            default:
                return ""
        }
        
    }
          
//    static func serviceAddOrRemoveDealFromWishlist(status: String, localDealID: String?, completion: @escaping (Bool, String) -> ()) {
//
//           var UrlFinal = APIConfig.BaseURL
//           var params = [String : Any]()
//
//           if status == "Add" {
//               params = ["dealId":localDealID ?? "", "userID":Session.getuserID()] as [String : Any]
//               UrlFinal = APIConfig.BaseURL + APIConfig.API_AddDealToWishlist
//
//           } else {
//               params = ["dealId":localDealID ?? "", "userID":Session.getuserID()] as [String : Any]
//               UrlFinal = APIConfig.BaseURL + APIConfig.API_RemoveDealFromList
//           }
//
//           Service.fetchGenericData(urlString: UrlFinal, params: params) { (success, localdeal_indexResponse:CommonResponse?) in
//               if success {
//
//                   if localdeal_indexResponse?.response == "Success" {
//                       if status == "Add" {
//                           completion(true, "Add")
//
//                       } else {
//                           completion(true, "Remove")
//
//                       }
//
//                    }  else {
//                        completion(false, "")
//
//                    }
//
//               } else {
//                    completion(false, "")
//
//               }
//           }
//    }
    
    
//    static func serviceAddOrRemoveCouponFromWishlist(status: String, couponID: String?, completion: @escaping (Bool, String) -> ()) {
//
//        var UrlFinal = APIConfig.BaseURL
//        UrlFinal = APIConfig.BaseURL + APIConfig.API_CouponInWallet
//
//        var params = [String : Any]()
//        let userID  = Session.getuserID()
//
//        if status == "Add" {
//            params = ["action":"add", "voucherId":couponID ?? "","userID":userID]
//
//        } else {
//            params = ["action":"remove", "voucherId":couponID ?? "","userID":userID]
//
//        }
//
//        Service.fetchGenericData(urlString: UrlFinal, params: params) { (success, voucher_indexResponse:CommonResponse?) in
//            if success {
//
//                if voucher_indexResponse?.response == "Success" {
//                    if status == "Add" {
//                        completion(true, "Add")
//
//                    } else {
//                        completion(true, "Remove")
//
//                    }
//
//                 }  else {
//                     completion(false, "")
//
//                 }
//
//            } else {
//                 completion(false, "")
//
//            }
//        }
//    }
    
    
    static func openMapForPlace(latitude: CLLocationDegrees, longitude:CLLocationDegrees, address: String) {
           let regionDistance:CLLocationDistance = 10000
           let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
           let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
           let options = [
               MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
               MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
           ]
           let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
           let mapItem = MKMapItem(placemark: placemark)
           mapItem.name = address
           mapItem.openInMaps(launchOptions: options)
    }
    
    static func returnSortKeyValue(sortOption: String) -> String {
        switch sortOption {
        case "trending":
            return "Trending"
            
        case "newarrivals":
            return "New Arrivals"
            
        case "pricehigh":
            return "Price: High to Low"
            
        case "pricelow":
            return "Price: Low to High"
            
        case "discount":
            return "Discount"
        default:
            return ""
        }
    }
    
    
//    static func loadResults(userID:String, cartID:String, completion: @escaping (Bool, CartViewResponse?) -> ()) {
//        let params = ["userID":userID,"cartID":cartID]
//
//        let urlViewCart = APIConfig.BaseURL + APIConfig.API_ViewCart
//        Service.fetchGenericData(urlString: urlViewCart, params: params) { (success, cartViewResponse:CartViewResponse?) in
//            if success {
//                if cartViewResponse?.response == "Failure" {
//                    //UserDefaults.standard.set("", forKey: "getcartQuantity")
//                    completion(false, nil)
//                    return
//                }
//
//                completion(true, cartViewResponse)
//                return
//
//            } else {
//                //ActivityIndicator.dismissActivityView()
//                completion(false, nil)
//
//            }
//        }
//    }
//
    
    static func returnSideMenuItemImage(item: String) -> String {
        
        switch item {
            case "Categories":
                return "browse-by-category"

//            case "Deals":
//                return "deal_2x"
//
//            case "Coupons":
//                return "coupon_2x"
//
//            case "Journals":
//                return "journal_2x"
//
//            case "Gift Coupon":
//                return "giftcoupon_2x"
//
//            case "Gift Card":
//                return "giftcard_2x"
//
//            case "Activities":
//                return "activities"

            case "Company":
                return "cribfee_3x"

            case "Business":
                return "company"

            case "Customer Support":
                return "customer-support"

            case "Sign Out":
                return "logout-menu"
            
            case "Contact Us":
                return "contact_2x"

            default:
                return "Radio-Button"
        }
    }
    
//    static func shareDealAction(userID:String, productId:String, completion: @escaping (Bool, CommonResponse?) -> ()) {
//           let params = ["userID":userID,"productID":productId]
//
//           let urlViewShareDeal = APIConfig.BaseURL + APIConfig.API_DEAL_SHARE
//           Service.fetchGenericData(urlString: urlViewShareDeal, params: params) { (success, commonResponse:CommonResponse?) in
//               if success {
//                   if commonResponse?.response == "Failure" || commonResponse?.response == "Error" {
//                       //UserDefaults.standard.set("", forKey: "getcartQuantity")
//                       completion(false, nil)
//                       return
//                   }
//
//                   completion(true, commonResponse)
//                   return
//
//               } else {
//                   //ActivityIndicator.dismissActivityView()
//                   completion(false, nil)
//
//               }
//           }
//    }
    
//    static func shareCouponAction(userID:String, voucherID:String, completion: @escaping (Bool, CommonResponse?) -> ()) {
//           let params = ["userID":userID,"voucherID":voucherID]
//
//           let urlViewShareCoupon = APIConfig.BaseURL + APIConfig.API_COUPON_SHARE
//           Service.fetchGenericData(urlString: urlViewShareCoupon, params: params) { (success, commonResponse:CommonResponse?) in
//               if success {
//                   if commonResponse?.response == "Failure" || commonResponse?.response == "Error" {
//                       //UserDefaults.standard.set("", forKey: "getcartQuantity")
//                       completion(false, nil)
//                       return
//                   }
//
//                   completion(true, commonResponse)
//                   return
//
//               } else {
//                   //ActivityIndicator.dismissActivityView()
//                   completion(false, nil)
//
//               }
//           }
//    }
//
    
//    static func changeTabbarIndex(index: Int) {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        let vc: UBMainTabbarVC = mainStoryboard.instantiateViewController(withIdentifier: "tabbarVC") as! UBMainTabbarVC
//        vc.selectedIndex = index
//
//        if let scene = UIApplication.shared.connectedScenes.first{
//            guard let windowScene = (scene as? UIWindowScene) else { return }
//            let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
//            window.windowScene = windowScene
//            window.rootViewController = vc
//            window.makeKeyAndVisible()
//            appDelegate.window = window
//        }
//    }
    
  /*  static func setTippedDate(date: String, bought: String) -> NSAttributedString {
        let attributedString1 = NSMutableAttributedString()
        let attrs1 = [NSAttributedString.Key.font : UIFont.Font(.medium, size: 16), NSAttributedString.Key.foregroundColor : UIColor.COLOR_GRAY_D]
        let attrs2 = [NSAttributedString.Key.font : UIFont.Font(.medium, size: 16), NSAttributedString.Key.foregroundColor : UIColor.COLOR_ORANGE]
        let attrs3 = [NSAttributedString.Key.font : UIFont.Font(.medium, size: 16), NSAttributedString.Key.foregroundColor : UIColor.COLOR_BLUE]
        
        attributedString1.append(NSAttributedString(string: "Tipped at, ", attributes:attrs1))
        attributedString1.append(NSAttributedString(string: "\(date) ", attributes:attrs2))
        attributedString1.append(NSAttributedString(string: "with ", attributes:attrs1))
        attributedString1.append(NSAttributedString(string: "\(bought) ", attributes:attrs3))
        attributedString1.append(NSAttributedString(string: "bought", attributes:attrs1))

        return attributedString1
    }
    
    
    static func setReturnPeriod(returnPeriod: String) -> NSAttributedString {
        let attrs1 = [NSAttributedString.Key.font : UIFont.Font(.medium, size: 16), NSAttributedString.Key.foregroundColor : UIColor.COLOR_GRAY_D]
        let attrs2 = [NSAttributedString.Key.font : UIFont.Font(.medium, size: 16), NSAttributedString.Key.foregroundColor : UIColor.COLOR_BLUE]

        let attributedString1 = NSMutableAttributedString()
        attributedString1.append(NSAttributedString(string: "Return Period : ", attributes:attrs1))
        attributedString1.append(NSAttributedString(string: "\(returnPeriod) ", attributes:attrs2))
        attributedString1.append(NSAttributedString(string: "Day(s)", attributes:attrs1))

        return attributedString1

        
    }
    */
    
//    static func serviceAddOrRemoveBusinessFromWishlist(status: String, businessID: String?, completion: @escaping (Bool, String) -> ()) {
//                   
//        var UrlFinal = APIConfig.BaseURL
//        UrlFinal = APIConfig.BaseURL + APIConfig.API_DEAL_FOLLOW
//      
//        var params = [String : Any]()
//        let userID  = Session.getuserID()
//
//        if status == "follow" {
//            params = ["action":status, "businessID":businessID ?? "","userID":userID]
//            
//        } else {
//            params = ["action":status, "businessID":businessID ?? "","userID":userID]
//
//        }
//        
//        Service.fetchGenericData(urlString: UrlFinal, params: params) { (success, voucher_indexResponse:CommonResponse?) in
//            if success {
//                
//                if voucher_indexResponse?.response == "Success" {
//                    if status == "Add" {
//                        completion(true, "Add")
//                        
//                    } else {
//                        completion(true, "Remove")
//
//                    }
//                   
//                 }  else {
//                     completion(false, "")
//
//                 }
//                
//            } else {
//                 completion(false, "")
//
//            }
//        }
//    }
    
    
   /* static func returnColorForBorder(value: Int) -> UIColor {
        switch value {
            case 0:
                return UIColor.COLOR_SLOT_PINK
                
            case 1:
                return UIColor.COLOR_SLOT_GREEN
                
            case 2:
                return UIColor.COLOR_SLOT_YELLOW
                
            case 3:
                return UIColor.COLOR_SLOT_ORANGE
                
            default:
                return UIColor.COLOR_SLOT_PINK
        }
    }
    
    static func returnMenuBackColor(value: Int) -> UIColor {
        switch value {
            case 0:
                return UIColor.COLOR_SLOT_PINK
                
            case 1:
                return UIColor.COLOR_SLOT_PURPLE
                
            case 2:
                return UIColor.COLOR_SLOT_YELLOW
                
            case 3:
                return UIColor.COLOR_SLOT_ORANGE
                
            default:
                return UIColor.COLOR_SLOT_PINK
        }
                       
    }
    */
}



