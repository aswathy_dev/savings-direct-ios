//
//  MacroFile.swift
//  Olvo
//
//  Created by Codcor on 26/03/19.
//  Copyright © 2019 Codcor Technologies. All rights reserved.
//

import Foundation
import Localize_Swift

let LZLoclized = "Localizable"

let NO_DATA = "No Data Found".localized(using: LZLoclized)
let ERROR_DATA = "ERROR IN GETTING VALUE".localized(using: LZLoclized)

let NETWORK_ERROR = "Error to Connect Network".localized(using: LZLoclized)

// User Default
let UDCountryCode           = "SelectedCountryCode"
let UDCountryID             = "SelectedCountryId"
let UDBalance               = "ShopBalance"
let UDMobileNumber          = "MobileNumber"
let UDAlreadyLogin          = "isAlreadyLogin"

//
let UDUserID                = "UserID"
let UDBussinessLocID        = "BussinessID"
let UDTabScreen             = "CurrentTabScreen"
let UDFromScreen            = "ScreenFrom"

let UDGoBackDash            = "GoBackToDashboard"

let UDLanguageCode          = "LanguageCode"

let UDLocTitle          = "LocationTitle"
let UDLocFullAddress    = "LocationFullAddress"
let UDLocphone          = "LocationPhoneNumber"
let UDLocEmail          = "LocationEmail"




// Web-service - Params
let WSUserID        = "userID"
let WSPageID        = "pageID"

let WSVoucherID     = "voucherId"

let WSStatus        = "status"
let WSStatusCode    = "status_code"
let WSSVerifyDavice = "verifyDevice"

let WSCountryCode   = "countryCode"
let WSMobile        = "mobile"
let WSDeviceID      = "deviceToken"

let WSError         = "error"
let WSMessage       = "message"
let WSTitle         = "title"
let WSData          = "data"

let WSAccessToken   = "access_token"
let WSTokenType     = "token_type"
let WSExpiresIn     = "expires_in"
let WSRefreshToken  = "refresh_token"
let WSScope         = "scope"
let WSShopID        = "shop_id"
let WSBrandID       = "brand_id"
let WSProductID     = "product_id"
let WSVarientID     = "varient_id"
let WSCountryID     = "country_id"
let WSRechargeType  = "recharge_type"
let WSUnitPrice     = "unit_price"
let WSCardCount     = "card_count"
let WSTotalAmount   = "total_amount"

let WSName          = "name"
let WSEmail         = "userEmail"
let WSRegion        = "region"
let WSID            = "_id"
let WSLogo          = "logo"
let WSCode          = "code"
let WS__V           = "__v"
let WSIcon          = "icon"
let WSType          = "type"
let WSMobileNumber  = "mobile_number"
let WSCountry_Code  = "country_code"
let WSCountry       = "country"
let WSCount         = "count"
let WSCheckSum      = "checksum"


let WSAmount            = "amount"
let WSWalletBalance     = "wallet_balance"
let WSCreditBalance     = "credit_balance"
let WSCredit            = "credit"
let WSTransactionNumber = "transaction_number"
let WSDiscountReceived  = "discount_received"
let WSSellRateCurrency  = "sell_rate_currency"
let WSSupportedDialer   = "supported_dialer"
let WSProductVarientID  = "product_varient_id"
let WSVoucherList       = "voucher_list"
let WSCardList          = "card_list"

let WSCreatedAt         = "createdAt"
let WSUpdatedAt         = "updatedAt"
let WSSerialNumber      = "serial_number"
let WSVoucherNumber     = "voucher_number"
let WSUserName          = "user_name"
let WSUserPassword          = "userPassword"
let WSTotalMinute       = "total_minute"
let WSDescription       = "description"

//Default
let DFImageDefault      = "place_holder"
let MainBoard           = "Main"
let DealCouponBoard     = "Deal_CouponBoard"


// Language Eng -> Fra
/// Name for language change notification

public let LCLLanguageChangeNotification = "LCLLanguageChangeNotification"


// let LGPopMsgUserName = "Enter the Valid User Name".localized(using: LZLoclized)
