//
//  Session.swift
//  Wishil
//
//  Created by Apple on 13/02/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class Session {
  
    static func getuserID() -> String{
        let userID = UserDefaults.standard.object(forKey: "userID") as? String
        return userID ?? ""
    }
    
    static func getlanguageID() -> String{
        let languageID = UserDefaults.standard.object(forKey: "languageID") as? String
        return languageID ?? ""
    }
    
    static func getcurrencySymbol() -> String{
        let getcurrencySymbol = UserDefaults.standard.object(forKey: "currencySymbol") as? String
        return getcurrencySymbol ?? "$"
    }
    static func getCountryID() -> String{
        let countryID  = UserDefaults.standard.object(forKey: "CountryID") as? String
        return countryID ?? ""
    }
    static func getCityID() -> String{
        let cityID  = UserDefaults.standard.object(forKey: "CityID") as? String
        return cityID ?? ""
    }
    static func getCityName() -> String{
        let cityName  = UserDefaults.standard.object(forKey: "CityName") as? String
        return cityName ?? "Select City"
    }
    static func getusertype() -> String{
        let userloginID = UserDefaults.standard.object(forKey: "usertype") as? String
        return userloginID ?? ""
    }
    static func launchFiirstTime() -> String{
        let userloginID = UserDefaults.standard.object(forKey: "launchFiirstTime") as? String
        return userloginID ?? "true"
    }
    static func getDeviceToken () -> String{
        
        let DeviceID = UserDefaults.UserDefaultsKeys.deviceID.rawValue
        let DeviceToken  = UserDefaults.standard.object(forKey: DeviceID) as? String
        return DeviceToken ?? ""
    }
    static func getcartID() -> String{
        let getcartID  = UserDefaults.standard.object(forKey: "cartID") as? String
        return getcartID ?? ""
    }
    
    static func getcartQuantity() -> String{
        let getcartID  = UserDefaults.standard.object(forKey: "getcartQuantity") as? String
        return getcartID ?? "0"
    }
    
    static func getuserName() -> String{
        let userName = UserDefaults.standard.object(forKey: "userName") as? String
        return userName ?? ""
    }
    
    static func getselectedLocation() -> String{
        let userName = UserDefaults.standard.object(forKey: "getselectedLocation") as? String
        return userName ?? ""
    }
    
    static func getCurrentCountry() -> String{
        let userName = UserDefaults.standard.object(forKey: "getCurrentCountry") as? String
        return userName ?? ""
    }
    
    static func getInstagramAccess_token() -> String{
        let userName = UserDefaults.standard.object(forKey: "getInstagramAccess_token") as? String
        return userName ?? ""
    }
    
    static func getProfileID() -> String{
        let userName = UserDefaults.standard.object(forKey: "ProfileUserID") as? String
        return userName ?? ""
    }
    
}
