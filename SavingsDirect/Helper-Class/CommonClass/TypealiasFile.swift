//
//  TypealiasFile.swift
//  Olvo
//
//  Created by Codcor on 27/03/19.
//  Copyright © 2019 Codcor Technologies. All rights reserved.
//

import Foundation
import UIKit

typealias CompletionReturnType = (Any?, Error?) -> Void

typealias ParamObj = [String : AnyObject]

let DefaultStd = UserDefaults.standard
