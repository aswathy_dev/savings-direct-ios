//
//  RootSwitcher.swift
//  SavingsDirect
//
//  Created by ISPG on 16/06/20.
//  Copyright © 2020 ISPG. All rights reserved.
//

import Foundation
import UIKit

class Switcher {
    
    static func updateRootVC(){
        
        let status = Session.getuserID()
        var rootVC : UIViewController?
       
        print(status)
        

        if Session.getuserID() != "" {
            rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SDMainTabBarVC") as! SDMainTabBarVC
            
        } else {
            rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SDLoginSignUpVC") as! SDLoginSignUpVC
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = rootVC
        appDelegate.window?.makeKeyAndVisible()

        
    }
    
}
