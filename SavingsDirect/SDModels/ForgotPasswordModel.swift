//
//  ForgotPasswordModel.swift
//  SavingsDirect
//
//  Created by ISPG on 16/06/20.
//  Copyright © 2020 ISPG. All rights reserved.
//

import Foundation

// MARK: - ForgotPasswordModel
struct ForgotPasswordModel: Codable {
    let response: String?
    let errorCode: Int?
    let errorMsg: String?
//    let currentLanguage: CurrentLanguage?
//    let settings: Settings?

    enum CodingKeys: String, CodingKey {
        case response, errorCode, errorMsg
//        case currentLanguage = "current_language"
//        case settings
    }
}

struct ChangePasswordModel: Codable {
    let response: String?
    let error: Int?
    let errorMsg: String?
}
