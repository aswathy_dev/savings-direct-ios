//
//  LoginSignUpModel.swift
//  SavingsDirect
//
//  Created by ISPG on 10/06/20.
//  Copyright © 2020 ISPG. All rights reserved.
//

import Foundation

// MARK: - LoginModel
struct LoginModel: Codable {
    let response, userID, userEmail, userActivated, userName: String?
    let profilepicpath: String?
    let errorMsg: String?
    let error: Int?
//    let currentLanguage: CurrentLanguage?
    let userFirstName, userLastName, userPhone: String? //###1

    enum CodingKeys: String, CodingKey {
        case response, userID, userEmail, userActivated, userName, profilepicpath, errorMsg, error, userFirstName, userLastName, userPhone
//        case currentLanguage = "current_language"
    }
}


// MARK: - Google and Facebook SignUpModel
struct SocialSignUpModel: Codable {
    
    let response, userID, isverify, userName: String?
    let userEmail, userActivated: String?
    let profilepicpath: String?
    let errorMsg: String?
//    let currentLanguage: CurrentLanguage?
//    let settings: Settings?

    enum CodingKeys: String, CodingKey {
        case response, userID, isverify, userName, userEmail, userActivated, profilepicpath, errorMsg
//        case currentLanguage = "current_language"
//        case settings
    }
}
