//
//  AppDelegate.swift
//  SavingsDirect
//
//  Created by ISPG on 08/06/20.
//  Copyright © 2020 ISPG. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import Localize_Swift
import UserNotifications
import IQKeyboardManager


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var devicetocken : String = ""

    // need to Add FCM Message_id For the App
    let gcmMessageIDKey = "326919093404-6ol8qe3lgc53m9eg1va8ais90vrhlpug.apps.googleusercontent.com"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        sleep(3)

        IQKeyboardManager.shared().isEnabled = true
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)

        FirebaseApp.configure()
        configureFirebase(application: application)
                
        GIDSignIn.sharedInstance().clientID = "326919093404-6ol8qe3lgc53m9eg1va8ais90vrhlpug.apps.googleusercontent.com" //"268847171221-1srmho2l66647bcvk0i8hk7em49gimde.apps.googleusercontent.com" //989947704994-44m7p35n4k7e3866qss9dg2jk9lf3k62.apps.googleusercontent.com
        GIDSignIn.sharedInstance().delegate = self
        
//        Switcher.updateRootVC()
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    
    private func configureFirebase (application:UIApplication) {
        
        UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }

        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            UNUserNotificationCenter.current().requestAuthorization(
                options: [.alert, .badge, .sound],
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
            
            
        Messaging.messaging().delegate = self
        Messaging.messaging().isAutoInitEnabled = true

                    
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                self.devicetocken = (result.token as NSString) as String
                UserDefaults.standard.setDeviceID(value: self.devicetocken)
//                UserDefaults.standard.set(self.devicetocken, forKey: "TOKEN")
                UserDefaults.standard.synchronize()
                print( "Remote InstanceID token: \(result.token)")
//                self.loadToken()
            }
        }
    }

}



extension AppDelegate: UNUserNotificationCenterDelegate, MessagingDelegate, GIDSignInDelegate {
    
    // Device Token
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        print("Firebase registration token: \(fcmToken)")
        
    }
    
    

    // recieve message
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let messageObj = notification.request.content.userInfo
        print(messageObj)
        
        completionHandler([.alert,.sound,.badge])
    }
        
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
     
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification

        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)

        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print(userInfo)

        let aps = userInfo["data"] as? [String:Any]
        let notificationType = aps?["notificationType"] as? String
        
        let notification = UILocalNotification()
        notification.userInfo = ["UUID": "reminderID" ]
        let state: UIApplication.State = UIApplication.shared.applicationState
        
        if state == .active{
            //            if let notification = userInfo["aps"] as? [AnyHashable: Any],
            //                let alert = notification["alert"] as? String {
            //                print(alert)
            let localNotification = UILocalNotification()
            localNotification.alertTitle = userInfo["title"] as? String
            localNotification.alertBody = userInfo["message"] as? String
            localNotification.alertLaunchImage = userInfo["image"] as? String
            localNotification.soundName = UILocalNotificationDefaultSoundName
            UIApplication.shared.scheduleLocalNotification(localNotification)
            //            }
        } else if state == .inactive{
            
            
            let localNotification = UILocalNotification()
            localNotification.alertTitle = userInfo["title"] as? String
            localNotification.alertBody = userInfo["message"] as? String
            localNotification.soundName = UILocalNotificationDefaultSoundName
            UIApplication.shared.scheduleLocalNotification(localNotification)
            
            //            }
        } else if state == .background{
            let localNotification = UILocalNotification()
            localNotification.alertTitle = userInfo["title"] as? String
            localNotification.alertBody = userInfo["message"] as? String
            localNotification.soundName = UILocalNotificationDefaultSoundName
            UIApplication.shared.scheduleLocalNotification(localNotification)
        }
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print(response.notification.request.content.userInfo)
        
//        var notificationModel = NotificationModel(from: <#Decoder#>)
//        notificationModel.title = response.notification.request.content.title
//        notificationModel.message = response.notification.request.content.body
//        notificationModel.image = response.notification.request.content.launchImageName
//        Service.notificationModel = notificationModel
        
        completionHandler()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        // Perform any operations on signed in user here.
        let userId = user.userID                  // For client-side use only!
        let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
        let givenName = user.profile.givenName
        let familyName = user.profile.familyName
        let email = user.profile.email
        // ...
    }
    
}
