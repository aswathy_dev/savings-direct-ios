//
//  ForgotPasswordViewModel.swift
//  SavingsDirect
//
//  Created by ISPG on 16/06/20.
//  Copyright © 2020 ISPG. All rights reserved.
//

import Foundation
import Alamofire

protocol PasswordDelegate {
    func SuccessForgotPassword(params : ParamObj, completion : @escaping CompletionReturnType)
}

class PasswordService : PasswordDelegate {
    
    func SuccessForgotPassword(params: ParamObj, completion: @escaping CompletionReturnType) {
        
        let URL = APIConfig.BaseURL + APIConfig.API_FORGOT
        
        let headers = ["Content-Type": "application/x-www-form-urlencoded", "Accept":"application/json"]
        
        Alamofire.request(URL, method: .post, parameters: params, encoding: URLEncoding.default, headers:headers).validate().responseJSON { response in
            print(response.request?.urlRequest ?? "")
            print(params)
            
            if let result = response.result.value {
                let JSON = result as! NSDictionary
                print(JSON)
            }
            
            guard let data = response.data else { return }
            do {
                let decoder = JSONDecoder()
                let Response = try decoder.decode(ForgotPasswordModel.self, from: data)
                completion(Response, nil)
                
            } catch let error {
                completion(nil, error)
            }
        }
    }
    
}
