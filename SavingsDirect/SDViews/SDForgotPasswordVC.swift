//
//  SDForgotPasswordVC.swift
//  SavingsDirect
//
//  Created by ISPG on 09/06/20.
//  Copyright © 2020 ISPG. All rights reserved.
//

import UIKit

class SDForgotPasswordVC: UIViewController {

    var ParamValue  = ParamObj()
    let viewModel   = PasswordService()

    @IBOutlet weak var txtValidEmail: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- Button actions
    @IBAction func actionSDGoBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func actionSDResetPassword(_ sender: UIButton) {
        
        guard let email = self.txtValidEmail.text, email.isValidEmail() else {
           self.showAlert(title: "Failed".localized(), msg: "Enter valid email address".localized())
           return
        }
       
        self.showSpinner(onView: self.view)

        ParamValue["emailID"] = email as AnyObject

        self.viewModel.SuccessForgotPassword(params: ParamValue) { (Result, Error) in

            self.removeSpinner()

            guard Error == nil else{
                self.showAlert(title: "", msg: "No Data Found.")
                return
            }

            let result = Result as! ForgotPasswordModel

            guard result.response != "Failure" else {
                self.showAlert(title: "", msg: result.errorMsg ?? NO_DATA)
                return
            }
            
            self.txtValidEmail.text = ""
            self.showAlertDismiss(title: "", msg: result.errorMsg ?? NO_DATA)
        
        }
    
    }
}
