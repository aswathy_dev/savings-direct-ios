//
//  SDLoginSignUpVC.swift
//  SavingsDirect
//
//  Created by ISPG on 09/06/20.
//  Copyright © 2020 ISPG. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
import FBSDKCoreKit
import AuthenticationServices


class SDLoginSignUpVC: UIViewController {

    var appleID         = String()
    var appleEmailID    = String()
    var appleFirstName  = String()
    var appleLastName   = String()
    
    var selGender: Int = -1
    var isLogin: Bool = true
    let viewModel    = AuthenticationService()
    var paramValue   = ParamObj()
    
    var DirectAuth : LoginModel? = nil
    var socialAuth : SocialSignUpModel? = nil
    
    @IBOutlet weak var lblIhaveAccount: UILabel!
    @IBOutlet weak var lblNewCustomer: UILabel!
    @IBOutlet weak var lblSignIn: UILabel!
    @IBOutlet weak var lblSignUp: UILabel!
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var vwSepLogin: UIView!
    @IBOutlet weak var vwSepSignUp: UIView!
    @IBOutlet weak var btnForgotPwd: UIButton!
    @IBOutlet weak var vwLoginView: UIView!
    @IBOutlet weak var vwSignUpView: UIView!
    @IBOutlet weak var vwReceiveEmail: UIView!

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtLoginEmail: UITextField!
    @IBOutlet weak var txtLoginPassword: UITextField!
    @IBOutlet weak var loginProviderStackView: UIStackView!
    @IBOutlet weak var signUpProviderStackView: UIStackView!

    
    @IBOutlet weak var txtSignUpFname: UITextField!
    @IBOutlet weak var txtSignUpLname: UITextField!
    @IBOutlet weak var txtSignUpEmail: UITextField!
    @IBOutlet weak var txtSignUpPhone: UITextField!
    @IBOutlet weak var txtSignUpPassword: UITextField!
    @IBOutlet weak var txtSignUpCPassword: UITextField!

    @IBOutlet weak var imgSelFemale: UIImageView!
    @IBOutlet weak var imgSelMale: UIImageView!

    @IBOutlet weak var signUpHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var loginVwHeightConstraint: NSLayoutConstraint!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupProviderLoginView()
        self.signUpHeightConstraint.constant = 750
        self.loginVwHeightConstraint.constant = 750
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

    }
    
    //MARK:- Apple Login
    
    @IBAction func doneBtn(_ sender: Any) {

        if txtEmail.text == "" {
            self.showAlert(title: "Sorry!", msg: "Please enter a valid email")
            return
        }

        appleEmailID = txtEmail.text ?? ""
        txtEmail.text = ""

        self.callAppleLoginService()

        self.vwReceiveEmail.isHidden = true

    }

    @IBAction func closeBtn(_ sender: Any) {
        self.vwReceiveEmail.isHidden = true

    }
    
    func setupProviderLoginView() {
        let signUpAppleButton = ASAuthorizationAppleIDButton()
        signUpAppleButton.addTarget(self, action: #selector(handleAuthorizationAppleIDButtonPress), for: .touchUpInside)
        self.signUpProviderStackView.addArrangedSubview(signUpAppleButton)

        let loginAppleButton = ASAuthorizationAppleIDButton()
        loginAppleButton.addTarget(self, action: #selector(handleAuthorizationAppleIDButtonPress), for: .touchUpInside)
        self.loginProviderStackView.addArrangedSubview(loginAppleButton)
    }
    
    @objc
    func handleAuthorizationAppleIDButtonPress() {
        let request = ASAuthorizationAppleIDProvider().createRequest()
        request.requestedScopes = [.fullName, .email]

        let controller = ASAuthorizationController(authorizationRequests: [request])
        controller.delegate = self
        controller.presentationContextProvider = self
        controller.performRequests()
    }
    
    func doAppleLogin(authData: ASAuthorizationAppleIDCredential) {
        
        let emailAddress = authData.email
        self.appleEmailID = emailAddress ?? ""
        self.appleID = authData.user
        self.appleFirstName = authData.fullName?.givenName ?? ""
        
        if self.appleEmailID == "" {
            self.vwReceiveEmail.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            self.vwReceiveEmail.isHidden = false
            return
        }
        
        self.callAppleLoginService()
    }

    func callAppleLoginService() {
        
        var params = ParamObj()
        params["email"] = appleEmailID as AnyObject
        params["id"]    =  appleID as AnyObject
        params["last_name"]  =  "" as AnyObject  //authData.fullName?.middleName as AnyObject
        params["first_name"] = appleFirstName as AnyObject
        params["cityID"] = Session.getCityID() as AnyObject
        
        self.callDirectAuthService(URL: APIConfig.API_APPLELOGN, params: params)
    }


    //MARK:- Button Actions
    @IBAction func actionSDSignIn(_ sender: UIButton) {
        self.isLogin = true
        self.vwLoginView.isHidden = !isLogin
        self.vwSignUpView.isHidden = isLogin
        
        self.vwSepLogin.backgroundColor = UIColor.COLOR_SD_BLUE_L
        self.vwSepSignUp.backgroundColor = UIColor.white
        
        self.signUpHeightConstraint.constant = 750
        self.loginVwHeightConstraint.constant = 750
        
        self.txtSignUpFname.text = ""
        self.txtSignUpLname.text = ""
        self.txtSignUpEmail.text = ""
        self.txtSignUpPhone.text = ""
        self.txtSignUpPassword.text = ""
        self.txtSignUpCPassword.text = ""
        
        self.selGender = -1
        self.imgSelMale.image = UIImage.init(named: "radiobut")
        self.imgSelFemale.image = UIImage.init(named: "radiobut")
        
    }
    
    @IBAction func actionSDSignUp(_ sender: UIButton) {
        self.isLogin = false
        self.vwLoginView.isHidden = !isLogin
        self.vwSignUpView.isHidden = isLogin
        
        self.vwSepLogin.backgroundColor = UIColor.white
        self.vwSepSignUp.backgroundColor = UIColor.COLOR_SD_BLUE_L

        self.signUpHeightConstraint.constant = 1050
        self.loginVwHeightConstraint.constant = 1050
        
        self.txtLoginEmail.text = ""
        self.txtLoginPassword.text = ""
    }
    
    @IBAction func actionSDForgotPassword(_ sender: UIButton) {
        
        
        
    }
    
    
    @IBAction func actionSDContinueAsGuest(_ sender: UIButton) {
           
           
           
    }
    
    
    @IBAction func actionSDFacebookLogin(_ sender: UIButton) {
        self.FacebookLogin()
    }
    
    @IBAction func actionSDGoogleLogin(_ sender: UIButton) {
        self.GoogleLogin()
    
    }
    
    @IBAction func actionSDSelectGender(_ sender: UIButton) {
        self.selGender = sender.tag
        
        var imgSetM = UIImage.init(named: "radiobut")
        var imgSetF = UIImage.init(named: "radiobut")
        
        if self.selGender == 10 {
            imgSetM = UIImage.init(named: "radiobut_active")
            imgSetF = UIImage.init(named: "radiobut")
            
        } else if self.selGender == 11 {
            imgSetM = UIImage.init(named: "radiobut")
            imgSetF = UIImage.init(named: "radiobut_active")
        }
    
        self.imgSelMale.image = imgSetM
        self.imgSelFemale.image = imgSetF
    }
    
    @IBAction func actionSDLoginSection(_ sender: UIButton) {
        self.LoginAuth()
    }
    
    
    @IBAction func actionSDSignUpSection(_ sender: UIButton) {
        self.SignUpAuth()
    }
    
    
    //MARK:-  Textfield Delegate
    
    
       
    //MARK: - Service Integration
    func callDirectAuthService( URL : String, params : ParamObj ){
           
       self.showSpinner(onView: self.view)
       self.viewModel.SuccessAuthentication(URL: URL, params: params) { (Result, Error) in
           
           guard Error == nil else{
               self.showAlert(title: "", msg: ERROR_DATA)
               self.removeSpinner()
               return }
           
           let result = Result as! LoginModel
           
           guard result.response != "Failure" else{
               self.showAlert(title: "", msg: result.errorMsg ?? "No Data Found.")
               self.removeSpinner()
               return
           }
           
           let UserID = result.userID ?? ""
           DefaultStd.setUserID(value: UserID)
           DefaultStd.setLoggedIn(value: true)
          /* DefaultStd.set(result.userName, forKey: "userName")
           DefaultStd.set(result.userFirstName ?? self.RegisterFName, forKey: "userFName")
           DefaultStd.set(result.userLastName ?? self.RegisterLName, forKey: "userLName")
           DefaultStd.set(result.userEmail, forKey: "userEmail")
           DefaultStd.set(result.userPhone ?? self.RegisterMobileNumber, forKey: "userPhone")
           DefaultStd.set(result.profilepicpath, forKey: "userProfile")*/

           self.removeSpinner()
//               self.registerNotification()
           
//           self.dismiss(animated: true)
        
        
            let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate
            sceneDelegate?.updateRootVC(scene: (sceneDelegate?.window?.windowScene)!)

       }
           
    }
    
    
    
    //MARK:- User defined functions
    
    func LoginAuth() {
            
        let LoginEmail = self.txtLoginEmail.text ?? ""
        let LoginPassword = self.txtLoginPassword.text ?? ""

        guard LoginEmail.isValidEmail() else {
            self.showAlert(title: "", msg: "Enter Valid Email Address")
            return
        }
        paramValue[WSEmail]    = LoginEmail as AnyObject
        
        guard LoginPassword.count > 6 else {
            self.showAlert(title: "", msg: "Enter Valid Password")
            return
        }
        paramValue[WSUserPassword]    = LoginPassword as AnyObject
        paramValue["cityID"]     = Session.getCityID() as AnyObject

        self.callDirectAuthService(URL: APIConfig.API_LOGIN, params: paramValue)
    }
       
    
    
    func SignUpAuth() {
            
        let RegisterFName = self.txtSignUpFname.text ?? ""
        let RegisterLName = self.txtSignUpLname.text ?? ""
        let RegisterMobileNumber = self.txtSignUpPhone.text ?? ""
        let RegisterEmail = self.txtSignUpEmail.text ?? ""

        let RegisterPassword = self.txtSignUpPassword.text ?? ""
        let RegisterConformPassword = self.txtSignUpCPassword.text ?? ""
        
        
        guard RegisterFName.count > 1 else {
            self.showAlert(title: "", msg: "Enter Valid First Name Address")
            return
        }
        paramValue["userFirstName"]    = RegisterFName as AnyObject
        
        guard RegisterLName.count > 1 else {
            self.showAlert(title: "", msg: "Enter Valid Last Name Address")
            return
        }
        paramValue["userLastName"]    = RegisterLName as AnyObject
        
        
        guard RegisterEmail.isValidEmail() else {
            self.showAlert(title: "", msg: "Enter Valid Email Address")
            return
        }
        paramValue["signupuserEmail"]    = RegisterEmail as AnyObject
        
        guard RegisterMobileNumber.count >= 9  else {
            self.showAlert(title: "", msg: "Enter Valid Mobile Number")
            return
        }
        paramValue["userPhone"]     = RegisterMobileNumber as AnyObject

        guard RegisterPassword.count > 6 else {
            self.showAlert(title: "", msg: "Enter Valid Password")
            return
        }
        paramValue["signupuserPassword"]    = RegisterPassword as AnyObject
        
        guard RegisterConformPassword == RegisterPassword else {
            self.showAlert(title: "", msg: "Password & Conform Password is not Match")
            return
        }
        paramValue["userConfirmPassword"]    = RegisterConformPassword as AnyObject
        
        if self.selGender == -1 {
            self.showAlert(title: "", msg: "Select Your Gender")
            return
        }
        
        if self.selGender == 10 {
            paramValue["userGender"] = "Male" as AnyObject
            
        } else {
            paramValue["userGender"] = "Female" as AnyObject

        }

//            guard isTermsChecked else {
//                self.showAlert(title: "", msg: "Check Our Terms and Conditions & Privacy Policy")
//                return
//            }
        
        paramValue["countryPhCode"] = "+1" as AnyObject
        paramValue["cityID"]     = Session.getCityID() as AnyObject

        self.callDirectAuthService(URL: APIConfig.API_REGISTER, params: paramValue)
    }
       
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}




extension SDLoginSignUpVC {
    
    func FacebookLogin() {
        
        let login = FBSDKLoginManager()
        login.logIn(withReadPermissions:  ["email"], from: self, handler: { result, error in
            if (!(error != nil)) {
                self.getFBUserData()
            }
           
        })
    }
        
   
    func getFBUserData() {
         //if (FBSDKAccessToken.current() != nil) {
          //   FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "first_name, last_name, picture.type(large), email, name, id, gender"]).start(completionHandler: { connection, result, error in

         if((FBSDKAccessToken.current()) != nil){
             FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "first_name, last_name, email, name, id"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil) {
                    print(result!)
                    self.showSpinner(onView: self.view)
                    var params = ParamObj()

                            // Handle vars
                    if let result = result as? [String : String],
                        let email   : String = result["email"],
                        let fbId    : String = result["id"],
                        let FName   : String = result["first_name"],
                        let LName   : String = result["last_name"] {

                        params["id"]    = fbId as AnyObject
                        params["email"] = email as AnyObject
                        params["last_name"]     = LName as AnyObject
                        params["first_name"]    = FName as AnyObject
                        params["cityID"]     = Session.getCityID() as AnyObject

                        self.removeSpinner()
                        self.callDirectAuthService(URL: APIConfig.API_FBLOGN, params: params)

                    }

                    self.removeSpinner()

                } else  {

                    let alertController = UIAlertController(title: "", message: NSLocalizedString("fberror", comment: " "), preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alertController, animated: true)
                }

             })
            
         } else {
            //FBSDKLoginManager().logOut()
            
         }
     }

    func GoogleLogin() {
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
        
    }
        
       
}


extension SDLoginSignUpVC : GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        var params = ParamObj()
        
        self.showSpinner(onView: self.view)
        guard error == nil else {
            self.showAlert(title: "Error to login with Google", msg: "")
            self.removeSpinner()
            return
        }
        
        params["id"]    = user.authentication.clientID as AnyObject
        params["email"] = user.profile.email as AnyObject
        
        params["given_name"]    = user.profile.givenName as AnyObject
        params["family_name"]   = user.profile.familyName as AnyObject
        params["cityID"]     = Session.getCityID() as AnyObject

        // params["gender"] = user.profile as AnyObject
        
        self.removeSpinner()
        self.callDirectAuthService(URL: APIConfig.API_GOOGLELOGIN, params: params)
        
    }
    
}


extension SDLoginSignUpVC: ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            let userIdentifier = appleIDCredential.user
        
            self.doAppleLogin(authData: appleIDCredential)

            
//            let defaults = UserDefaults.standard
//            defaults.set(userIdentifier, forKey: "userIdentifier1")
//
//            //Save the UserIdentifier somewhere in your server/database
//            let vc = UserViewController()
//            vc.userID = userIdentifier
//            self.present(UINavigationController(rootViewController: vc), animated: true)
            break
        default:
            break
        }
    }
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
           return self.view.window!
    }
}
