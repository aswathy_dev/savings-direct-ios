//
//  APIManager.swift
//  Oorlit
//
//  Created by ispg on 16/10/19.
//  Copyright © 2019 ISPG technologies india pvt ltd. All rights reserved.
//

import Foundation
import UIKit

struct APIConfig {
    
	// SWIFT_ACTIVE_COMPILATION_CONDITIONS = DEBUG SO StagingURL EXECUTE
    
    static var SERVERURL: String {
        return "https://stg70.oorjit.net/savings-direct/" //"https://stg70.oorjit.net/slot-booking-v2/"
    }
    
    static var BaseURL: String {
        
        #if UAT
        return "https://stg70.oorjit.net/ummabee/mobileapp/" // ProductionURL
        #else
//        return "https://stg70.oorjit.net/ummabee.com/mobileapp/" // StagingURL
//        return "https://staging.ummabee.com/mobileapp/"
        return "https://stg70.oorjit.net/savings-direct/mobileapp/"//"https://stg70.oorjit.net/slot-booking-v2/mobileapp/"
        #endif
    }

    static let API_LOGIN                     = "user/index/"
    static let API_REGISTER                  = "user/signup/"

    static let API_FORGOT                    = "index/forgotPassword/"

    static let API_RESET                     = "user/password/"

    static let API_LOCATION                  = "--"


    static let API_FBLOGN                    = "user/fbUserSignup/"
    static let API_GOOGLELOGIN               = "googleplus/index/"
    static let API_APPLELOGN                = "user/appleLogin/"

    static let API_FAVOURITE_VOUCHER         = "user/voucherfav"
    static let API_FAVOURITE_SHOP            = "user/favourite"
    static let API_FAVOURITE_DEALS           = "user/wishlist/"


    static let API_UNFAVOURITE               = "user/removeFromCouponWallet"
    static let API_REDEEM_COUPON             = "user/redeemedCoupons/"

    static let API_NOTIFICATION              = "user/notifications/"

    static let API_REG_NOTIFICATION         = "push/index/"

    static let API_ENQUIRUES                 = "user/ticketList/"

    static let API_ENQUIRUES_DETAIL          = "user/ticketDetails/"

    static let API_ENQUIRUES_POST            = "user/ticketDetails/"

    static let API_ENQUIRUES_REOPEN          = "user/ticketDetails/"

    static let API_ENQUIRUES_CLOSE           = "user/ticketDetails/"
    
    static let API_REFER_AND_EARN            = "User/referrals"


    static let API_DEAL_PURCHESES_INVOICE    = "user/invoice/"

    static let API_ACCOUNT_SETTING           = "user/editaccount/"

    static let API_DEAL_PURCHESES            = "user/purchases/"
    
    static let API_DEAL_CANCEL_LIST          = "user/rma"
    
    static let API_DEAL_REATURN_REQUEST      = "user/returnrequest"
    
    static let API_DEAL_REATURN_DETAIL       = "user/returndetails/"

    static let API_ACCOUNT                   = "user/account/"

    static let API_Gift_Card                 = "giftcard/index/"

    static let API_List_Widget               = "index/listwidget"

    static let API_WALLET                    = "user/wallet/"

    static let API_CASHBACK                  = "user/cashback"

    static let API_SAVING_SUMMARY            = "user/savingsummary"

    static let API_SUMMARY_CHART             = "user/savingsummarygraph"

    static let API_DEAL_PREFERECNE           = "user/dealpreference"

    static let API_DEAL_PREFERECNE_CITY      = "user/citiesList/"

    static let API_DEAL_FOLLOW               = "user/following/"
    
    static let API_DEAL_UNFOLLOW             = "user/removeFromWallet/"

    static let API_RecentlyViewed            = "index/recentltyViewedDeals/"

    static let API_DealList                  = "Localdeal/index"
    static let API_ProductList               = "products/index/"
    static let API_GetawayList               = "getaways/index/"

    static let API_LocalDealDetail           = "localdeal/dealdetail/"
    static let API_GetawayDealDetail         = "getaways/deal/"
    static let API_ProductDealDetail         = "products/deal/"

    static let API_SearchPlatform            = "index/autoSuggestDeal"

    static let API_AddToCart                 = "cart/addItem/"
    static let API_AddDealToWishlist         = "user/addToWallet" //"user/wishlist"
    static let API_RemoveDealFromList        = "user/removeFromWallet"

    static let API_DealReview                = "Dealreview/index/"

    static let API_GetDealSlotValues         = "localdeal/getDealSlotValues"

    static let API_IndexCityAll              = "index/cityAll/"
    static let API_PollIndex                 = "poll/index/"

    static let API_DEAL_CERTIFICATE          = "user/certificates/"

    static let API_CERTIFICATE_TRANSFER      = "user/transferToFriend/"

    static let API_CERTIFICATE_STATUS        = "user/certificatesStatus/"



    static let API_Customer_Service          = "index/customerservice"
    static let API_Subscribe                 = "index/subscribe/"
    static let API_ScratchCard               = "dailyscratch/index/"
    static let API_LeaderBoard               = "Leadershipboard/index/"
    static let API_ContactUS                 = "Contactus/index"
    static let API_Suggest_business          = "index/suggestABusiness"
    static let API_CountryCodeList           = "user/getCountryCode"
    static let API_QuizList                  = "quiz/"

    static let API_StoreList                 = "store/index/"

    static let API_REQUEST_A_DEAL            = "Deal/RequestADeal"

    static let API_BUSINESS_DETAIL           = "store/businessDetails/"

    static let API_SetScratch                = "dailyscratch/setUserScratchVoucher"
    static let API_SpinnerView               = "spinner"
    static let API_Request_Support           = "index/ticketadd"
    static let API_CMS_Service               = "index/cms"
    static let API_Contact_Submit            = "Contactus/index"
    static let API_Department_list           = "index/departmentList"


    static let API_MY_REWARDS                = "user/rewards/"

    static let API_MY_REWARDS_DETAIL         = "user/redeemList/"

    static let API_CityDetails               = "index/city/"
    static let API_GetAirportData            = "getaways/getSlotValues/languageIDs/1"
    static let API_IndexMenu                 = "index/homeMenus"
    static let API_CartExpress               = "Cart/expresscheckout"
    static let API_RedeemPoint               = "cart/redeemPoints/"
    static let API_PaymentSetting            = "Cart/paymentGatewaySettings"
    static let API_Cart_UserCredit           = "cart/useUserCredit/"
    static let API_CouponIndex               = "coupon/index/"
    static let API_CouponInWallet            = "coupon/couponaddToWallet/"
    static let API_SearchList                = "search/list"

    static let API_Filter_Gift_Card          = "giftcard/listFilter/"

    static let API_Filter_Gift_Card_Detail   = "giftcard/view/"

    static let API_Send_Gift_card_List       = "user/giftcards"

    static let API_Send_Gift_Card_Details    = "giftcard/cardinvoice/"

    static let API_Country_List              = "Localdeal/countriesList"

    static let API_States_List               = "Localdeal/StateList"

    static let API_save_gift_card            = "giftcard/saveGiftCouponDetails/"

    static let API_payment_confirmation      = "giftcard/payment/"

    static let API_get_gift_coupon           = "giftcoupon/index/"

    static let API_gift_coupon_payment       = "giftcoupon/payment/"

    static let API_gift_coupon_redeem        = "giftcoupon/redeem"

    static let API_gift_coupon_order         = "user/giftpurchases"

    static let API_gift_coupon_order_details = "user/invoice"

    static let API_ViewCart                  = "cart/viewCart/"
    static let API_JournalList               = "journal/list/"
    static let API_JournalDetail             = "journal/show/"
    static let API_CouponSendEmail           = "coupon/emailSend/"
    static let API_CouponDetail              = "coupon/detail/"
    static let API_CartPayment               = "Cart/payment/"
    static let API_UpdateCart                = "cart/updatecart/"
    static let API_CartUpdateAddress         = "Cart/updateAddress/"
    static let API_CountriesList             = "Localdeal/countriesList"
    static let API_StateList                 = "Localdeal/stateList"
    static let API_GetCountryCode            = "user/getCountryCode"
    static let API_CartIndex                 = "Cart/index"
    static let API_UserShipping              = "user/shipping/"
    static let API_ListPaymentMethods        = "cart/listPaymentmethods/"
    static let API_CMS_INDEX                 = "index/cms/"
    static let API_COUPON_SHARE              = "coupon/setLoyaltyShare/"
    static let API_DEAL_SHARE                = "deal/setLoyaltyShare/"
    static let API_MAPVIEW                   = "Localdeal/map/"
    static let API_GETSLOT_TIME             = "localdeal/getDealSlotValues/"
    static let API_BANKLIST                 = "index/listPaymentBankTypes"
    static let API_UPDATE_SLOT              = "user/updateSlot/"
}

